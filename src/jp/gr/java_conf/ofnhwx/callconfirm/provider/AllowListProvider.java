package jp.gr.java_conf.ofnhwx.callconfirm.provider;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import jp.gr.java_conf.ofnhwx.callconfirm.provider.AllowList.List;
import jp.gr.java_conf.ofnhwx.olib.base.BaseContentProvider;
import jp.gr.java_conf.ofnhwx.olib.base.BaseSQLiteOpenHelper;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

/**
 * 許可リストのプロバイダ.
 * @author yuta
 */
public class AllowListProvider extends BaseContentProvider {

    private static final String LIST_TABLE_NAME = "list";

    private static final int LIST    = 1;
    private static final int LIST_ID = 2;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static final Map<String, String> sListProjectionMap = new HashMap<String, String>();
    static {
        sUriMatcher.addURI(AllowList.AUTHORITY, "list"  , LIST   );
        sUriMatcher.addURI(AllowList.AUTHORITY, "list/#", LIST_ID);
        sListProjectionMap.put(List._ID   , List._ID   );
        sListProjectionMap.put(List.NAME  , List.NAME  );
        sListProjectionMap.put(List.NUMBER, List.NUMBER);
    }

    private DatabaseHelper mOpenHelper;

    private static class DatabaseHelper extends BaseSQLiteOpenHelper {
        public DatabaseHelper(Context context) {
            super(context, AllowList.DB_NAME, null, AllowList.DB_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            LinkedHashMap<String, SQLiteType> columns = new LinkedHashMap<String, BaseSQLiteOpenHelper.SQLiteType>();
            columns.put(List._ID   , SQLiteType.ID  );
            columns.put(List.NAME  , SQLiteType.TEXT);
            columns.put(List.NUMBER, SQLiteType.TEXT);
            createTable(db, LIST_TABLE_NAME, columns);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            deleteTable(db, LIST_TABLE_NAME);
            onCreate(db);
        }
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        switch (sUriMatcher.match(uri)) {
        case LIST:
            break;
        case LIST_ID:
            where = whereWithId(uri, where);
            break;
        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count = db.delete(LIST_TABLE_NAME, where, whereArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        switch (sUriMatcher.match(uri)) {
        case LIST:
            break;
        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (values == null) {
            values = new ContentValues();
        }
        check_and_set(values, List.NAME  , (String)null);
        check_and_set(values, List.NUMBER, (String)null);
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long rowId = db.insert(LIST_TABLE_NAME, null, values);
        if (rowId > 0) {
            Uri result = ContentUris.withAppendedId(List.CONTENT_URI, rowId);
            getContext().getContentResolver().notifyChange(result, null);
            return result;
        } else {
            throw new SQLException("Failed to insert row into " + uri);
        }
     }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        switch (sUriMatcher.match(uri)) {
        case LIST:
            qb.setTables(LIST_TABLE_NAME);
            qb.setProjectionMap(sListProjectionMap);
            break;
        case LIST_ID:
            qb.setTables(LIST_TABLE_NAME);
            qb.setProjectionMap(sListProjectionMap);
            qb.appendWhere(whereWithId(uri));
            break;
        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (TextUtils.isEmpty(sortOrder)) {
            sortOrder = List.DEFAULT_SORT_ORDER;
        }
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        switch (sUriMatcher.match(uri)) {
        case LIST:
            break;
        case LIST_ID:
            where = whereWithId(uri, where);
            break;
        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count = db.update(LIST_TABLE_NAME, values, where, whereArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

}
