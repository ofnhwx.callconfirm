package jp.gr.java_conf.ofnhwx.callconfirm.provider;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Prefix/Suffixの項目
 * @author yuta
 */
public class EditNumber {
    public static final String AUTHORITY  = "jp.gr.java_conf.ofnhwx.CallConfirm.EditNumber";
    public static final String DB_NAME    = "editnumber.db";
    public static final int    DB_VERSION = 1;
    public static final class List implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/list");
        public static final String DEFAULT_SORT_ORDER = "name ASC";
        public static final String NAME   = "name";
        public static final String PREFIX = "prefix";
        public static final String SUFFIX = "suffix";
        public static final String OPTION = "option";
    }
}
