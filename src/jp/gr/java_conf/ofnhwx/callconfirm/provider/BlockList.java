package jp.gr.java_conf.ofnhwx.callconfirm.provider;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * ブロックリストの項目.
 * @author yuta
 */
public final class BlockList {
    public static final String AUTHORITY  = "jp.gr.java_conf.ofnhwx.CallConfirm.BlockList";
    public static final String DB_NAME    = "blocklist.db";
    public static final int    DB_VERSION = 2;
    public static final class List implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/list");
        public static final String DEFAULT_SORT_ORDER = "number ASC";
        public static final String NUMBER = "number";
        public static final String COUNT  = "count";
        public static final String TIME   = "time";
    }
    public static final class History implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/history");
        public static final String DEFAULT_SORT_ORDER = "time DESC";
        public static final String NUMBER = "number";
        public static final String TIME   = "time";
    }
}
