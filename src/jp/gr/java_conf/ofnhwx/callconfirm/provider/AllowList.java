package jp.gr.java_conf.ofnhwx.callconfirm.provider;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * 許可リストの項目.
 * @author yuta
 */
public class AllowList {
    public static final String AUTHORITY  = "jp.gr.java_conf.ofnhwx.CallConfirm.AllowList";
    public static final String DB_NAME    = "allowlist.db";
    public static final int    DB_VERSION = 1;
    public static final class List implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/list");
        public static final String DEFAULT_SORT_ORDER = "name ASC";
        public static final String NAME   = "name"  ;
        public static final String NUMBER = "number";
    }
}
