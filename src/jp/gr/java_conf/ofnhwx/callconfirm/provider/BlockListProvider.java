package jp.gr.java_conf.ofnhwx.callconfirm.provider;

import java.util.HashMap;
import java.util.LinkedHashMap;

import jp.gr.java_conf.ofnhwx.callconfirm.provider.BlockList.History;
import jp.gr.java_conf.ofnhwx.callconfirm.provider.BlockList.List;
import jp.gr.java_conf.ofnhwx.olib.base.BaseContentProvider;
import jp.gr.java_conf.ofnhwx.olib.base.BaseSQLiteOpenHelper;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

/**
 * ブロックリストのプロバイダ.
 * @author yuta
 */
public class BlockListProvider extends BaseContentProvider {

    private static final String LIST_TABLE_NAME    = "list";
    private static final String HISTORY_TABLE_NAME = "history";

    private static final int LIST       = 1;
    private static final int LIST_ID    = 2;
    private static final int HISTORY    = 3;
    private static final int HISTORY_ID = 4;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static final HashMap<String, String> sListProjectionMap     = new HashMap<String, String>();
    private static final HashMap<String, String> sHistoryProjectionMap  = new HashMap<String, String>();
    static {
        sUriMatcher.addURI(BlockList.AUTHORITY, "list"     , LIST      );
        sUriMatcher.addURI(BlockList.AUTHORITY, "list/#"   , LIST_ID   );
        sUriMatcher.addURI(BlockList.AUTHORITY, "history"  , HISTORY   );
        sUriMatcher.addURI(BlockList.AUTHORITY, "history/#", HISTORY_ID);
        sListProjectionMap.put(List._ID   , List._ID   );
        sListProjectionMap.put(List.NUMBER, List.NUMBER);
        sListProjectionMap.put(List.COUNT , List.COUNT );
        sListProjectionMap.put(List.TIME  , List.TIME  );
        sHistoryProjectionMap.put(History._ID   , History._ID   );
        sHistoryProjectionMap.put(History.NUMBER, History.NUMBER);
        sHistoryProjectionMap.put(History.TIME  , History.TIME  );
    }

    private DatabaseHelper mOpenHelper;

    private static class DatabaseHelper extends BaseSQLiteOpenHelper {
        public DatabaseHelper(Context context) {
            super(context, BlockList.DB_NAME, null, BlockList.DB_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            createBlockListTable(db);
            createBlockHistoryTable(db);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            if (oldVersion == 1 && newVersion == 2) {
                createBlockHistoryTable(db);
                return;
            }
            deleteTable(db, LIST_TABLE_NAME   );
            deleteTable(db, HISTORY_TABLE_NAME);
            createBlockListTable   (db);
            createBlockHistoryTable(db);
        }
        private void createBlockListTable(SQLiteDatabase db) {
            LinkedHashMap<String, SQLiteType> columns = new LinkedHashMap<String, BaseSQLiteOpenHelper.SQLiteType>();
            columns.put(List._ID   , SQLiteType.ID);
            columns.put(List.NUMBER, SQLiteType.TEXT);
            columns.put(List.COUNT , SQLiteType.INTEGER);
            columns.put(List.TIME  , SQLiteType.INTEGER);
            createTable(db, LIST_TABLE_NAME, columns);
        }
        private void createBlockHistoryTable(SQLiteDatabase db) {
            LinkedHashMap<String, SQLiteType> columns = new LinkedHashMap<String, BaseSQLiteOpenHelper.SQLiteType>();
            columns.put(History._ID   , SQLiteType.ID);
            columns.put(History.NUMBER, SQLiteType.TEXT);
            columns.put(History.TIME  , SQLiteType.INTEGER);
            createTable(db, HISTORY_TABLE_NAME, columns);
        }
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        switch (sUriMatcher.match(uri)) {
        case LIST:
            qb.setTables(LIST_TABLE_NAME);
            qb.setProjectionMap(sListProjectionMap);
            break;
        case LIST_ID:
            qb.setTables(LIST_TABLE_NAME);
            qb.setProjectionMap(sListProjectionMap);
            qb.appendWhere(whereWithId(uri));
            break;
        case HISTORY:
            qb.setTables(HISTORY_TABLE_NAME);
            qb.setProjectionMap(sHistoryProjectionMap);
            break;
        case HISTORY_ID:
            qb.setTables(HISTORY_TABLE_NAME);
            qb.setProjectionMap(sHistoryProjectionMap);
            qb.appendWhere(whereWithId(uri));
            break;
        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (TextUtils.isEmpty(sortOrder)) {
            switch (sUriMatcher.match(uri)) {
            case LIST:
            case LIST_ID:
                sortOrder = List.DEFAULT_SORT_ORDER;
                break;
            case HISTORY:
            case HISTORY_ID:
                sortOrder = History.DEFAULT_SORT_ORDER;
                break;
            }
        }
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if (values == null) {
            values = new ContentValues();
        }
        String tableName;
        Uri contentUri;
        switch (sUriMatcher.match(uri)) {
        case LIST:
            check_and_set(values, List.NUMBER, (String)null);
            check_and_set(values, List.COUNT , 0L);
            check_and_set(values, List.TIME  , 0L);
            tableName = LIST_TABLE_NAME;
            contentUri = BlockList.List.CONTENT_URI;
            break;
        case HISTORY:
            check_and_set(values, History.NUMBER, (String)null);
            check_and_set(values, History.TIME  , 0L);
            tableName = HISTORY_TABLE_NAME;
            contentUri = BlockList.History.CONTENT_URI;
            break;
        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long rowId = db.insert(tableName, null, values);
        if (rowId > 0) {
            Uri result = ContentUris.withAppendedId(contentUri, rowId);
            getContext().getContentResolver().notifyChange(result, null);
            return result;
        } else {
            throw new SQLException("Failed to insert row into " + uri);
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        String tableName;
        switch (sUriMatcher.match(uri)) {
        case LIST:
            tableName = LIST_TABLE_NAME;
            break;
        case LIST_ID:
            tableName = LIST_TABLE_NAME;
            where = whereWithId(uri, where);
            break;
        case HISTORY:
            tableName = HISTORY_TABLE_NAME;
            break;
        case HISTORY_ID:
            tableName = HISTORY_TABLE_NAME;
            where = whereWithId(uri, where);
            break;
        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count = db.update(tableName, values, where, whereArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        String tableName;
        switch (sUriMatcher.match(uri)) {
        case LIST:
            tableName = LIST_TABLE_NAME;
            break;
        case LIST_ID:
            tableName = LIST_TABLE_NAME;
            where = whereWithId(uri, where);
            break;
        case HISTORY:
            tableName = HISTORY_TABLE_NAME;
            break;
        case HISTORY_ID:
            tableName = HISTORY_TABLE_NAME;
            where = whereWithId(uri, where);
            break;
        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count = db.delete(tableName, where, whereArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

}
