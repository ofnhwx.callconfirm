package jp.gr.java_conf.ofnhwx.callconfirm.utility;

import jp.gr.java_conf.ofnhwx.olib.compatibility.OCompatibility.Version;
import android.content.Context;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;

/**
 * Bluetoothの状態を判定する.<br>
 * <ul>
 *   <li>Donut(1.6)以前
 *     <pre>Bluetooth設定の有効無効で判定する</pre>
 *   </li>
 *   <li>Eclair(2.0,2.1)以降
 *     <pre>未完成:Bluetooth機器の接続状態で判定する(失敗した場合はDonut(1.6)以前と同じ</pre>
 *   </li>
 * </ul>
 * @author yuta
 */
public abstract class BluetoothTest {

    /**
     * Bluetoothの有効・無効を取得.
     * @param context
     * @return true:有効 / false:無効
     */
    public static final boolean isEnabled(Context context) {
        if (Version.isAndroid21()) {
            return isEnabled_5(context);
        } else {
            return isEnabled_4(context);
        }
    }

    private static final boolean isEnabled_5(Context context) {
        return isEnabled_4(context);
    }

    private static final boolean isEnabled_4(Context context) {
        try {
            return Secure.getInt(context.getContentResolver(), Secure.BLUETOOTH_ON) != 0;
        } catch (SettingNotFoundException e) {
            return false;
        }
    }

}
