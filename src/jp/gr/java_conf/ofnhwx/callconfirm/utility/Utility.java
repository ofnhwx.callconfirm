package jp.gr.java_conf.ofnhwx.callconfirm.utility;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.olib.compatibility.OContacts;
import jp.gr.java_conf.ofnhwx.olib.utils.OUtil;
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Vibrator;

import com.android.internal.telephony.ITelephony;

/**
 * その他の関数をまとめたクラス.
 * @author yuta
 */
public abstract class Utility {

    /**
     * ITelephonyのインスタンスを取得
     * @param context
     * @return ITelephony, 失敗したら'null'
     */
    public static final ITelephony connectToTelephonyService(Context context) {
        return (ITelephony)OUtil.connectToTelephonyService(context);
    }

    /**
     * バイブレーション.
     * @param context
     */
    public static final void vibrate(Context context) {
        Vibrator vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
        long[]   patterns = Param.getVibrationPattern(context);
        if (patterns.length > 1) {
            vibrator.vibrate(patterns, -1);
        } else {
            vibrator.vibrate(patterns[0]);
        }
    }

    /**
     * 電話帳から電話番号を取得するための補助.
     * @param context
     * @param data
     * @return
     */
    public static final String getNumberFromIntent(Context context, Intent data) {
        ContentResolver cr = context.getContentResolver();
        long id;
        // UriからIDを取得
        Cursor c = cr.query(data.getData(), null, null, null, null);
        if (c == null) {
            return null;
        }
        try {
            c.moveToFirst();
            return c.getString(c.getColumnIndexOrThrow(OContacts.NUMBER));
        } catch (IllegalArgumentException e) {
            id = c.getLong(c.getColumnIndex(OContacts.ID));
        } finally {
            c.close();
        }
        // IDから電話番号を取得
        Uri uri = ContentUris.withAppendedId(OContacts.PHONES_CONTENT_URI, id);
        c = cr.query(uri, null, null, null, null);
        if (c == null) {
            return null;
        }
        try {
            c.moveToFirst();
            return c.getString(c.getColumnIndexOrThrow(OContacts.NUMBER));
        } catch (IllegalArgumentException e) {
            // TODO:エラーの変わりに何かメッセージを出す(とりあえずはエラーのまま)
            throw e;
        } finally {
            c.close();
        }
    }

    /**
     * MODIFY_PHONE_STATE権限がない場合に警告を表示する.
     */
    public static final void showGingerbreadUsersSecurityPopup(final Activity activity) {
        // 許可されている場合は何もしない
        if (OUtil.hasPermission(activity, Manifest.permission.MODIFY_PHONE_STATE)) {
            return;
        }
        // 許可されていない場合はダイアログを表示
        // 実際はよくない方法だけど許してね？
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.setTitle(android.R.string.dialog_alert_title);
        dialog.setMessage(R.string.alert_text);
        dialog.setPositiveButton("Open Issue 15031", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Uri uri = Uri.parse("http://code.google.com/p/android/issues/detail?id=15031");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                activity.startActivity(intent);
            }
        });
        dialog.setNegativeButton("Close", null);
        dialog.show();
    }

}
