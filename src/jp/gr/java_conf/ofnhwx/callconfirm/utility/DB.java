package jp.gr.java_conf.ofnhwx.callconfirm.utility;

import jp.gr.java_conf.ofnhwx.callconfirm.provider.AllowList;
import jp.gr.java_conf.ofnhwx.callconfirm.provider.BlockList;
import jp.gr.java_conf.ofnhwx.callconfirm.provider.EditNumber;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.info.BlockListInfo;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;

/**
 * DBの簡易編集.
 * @author yuta
 */
public abstract class DB {
    private static final String x(String number) {
        if (TextUtils.isEmpty(number)) {
            return "";
        } else {
            return PhoneNumberUtils.stripSeparators(number);
        }
    }
    /**
     * 許可リストの簡易編集.
     */
    public static class AllowListItem {
        public static final void insert(Context context, String name, String number) {
            ContentResolver cr = context.getContentResolver();
            ContentValues values = new ContentValues();
            values.put(AllowList.List.NAME  , name);
            values.put(AllowList.List.NUMBER, DB.x(number));
            cr.insert(AllowList.List.CONTENT_URI, values);
        }
        public static final void update(Context context, long id, String name, String number) {
            ContentResolver cr = context.getContentResolver();
            ContentValues values = new ContentValues();
            Uri uri = ContentUris.withAppendedId(AllowList.List.CONTENT_URI, id);
            values.put(AllowList.List.NAME  , name);
            values.put(AllowList.List.NUMBER, DB.x(number));
            cr.update(uri, values, null, null);
        }
        public static final void delete(Context context, long id) {
            ContentResolver cr = context.getContentResolver();
            Uri uri = ContentUris.withAppendedId(AllowList.List.CONTENT_URI, id);
            cr.delete(uri, null, null);
        }
        public static final void deleteAll(Context context) {
            ContentResolver cr = context.getContentResolver();
            cr.delete(AllowList.List.CONTENT_URI, null, null);
        }
    }
    /**
     * ブロックリストの簡易編集.
     */
    public static class BlockListItem {
        public static final void addCount(Context context, String number, long time) {
            BlockListInfo info = new BlockListInfo(context, number);
            if (info.isBlockNumber()) {
                ContentResolver cr = context.getContentResolver();
                Uri uri = ContentUris.withAppendedId(BlockList.List.CONTENT_URI, info.getId());
                ContentValues values = new ContentValues();
                values.put(BlockList.List.COUNT, info.getCount() + 1);
                values.put(BlockList.List.TIME , time);
                cr.update(uri, values, null, null);
            }
        }
        public static final void insert(Context context, String number) {
            ContentResolver cr = context.getContentResolver();
            ContentValues values = new ContentValues();
            values.put(BlockList.List.NUMBER, DB.x(number));
            values.put(BlockList.List.TIME  , System.currentTimeMillis());
            values.put(BlockList.List.COUNT , 0);
            cr.insert(BlockList.List.CONTENT_URI, values);
        }
        public static final void update(Context context, long id, String number, long time, long count) {
            ContentResolver cr = context.getContentResolver();
            ContentValues values = new ContentValues();
            Uri uri = ContentUris.withAppendedId(BlockList.List.CONTENT_URI, id);
            values.put(BlockList.List.NUMBER, DB.x(number));
            values.put(BlockList.List.TIME  , time );
            values.put(BlockList.List.COUNT , count);
            cr.update(uri, values, null, null);
        }
        public static final void delete(Context context, long id) {
            ContentResolver cr = context.getContentResolver();
            Uri uri = ContentUris.withAppendedId(BlockList.List.CONTENT_URI, id);
            cr.delete(uri, null, null);
        }
        public static final void deleteAll(Context context) {
            ContentResolver cr = context.getContentResolver();
            cr.delete(BlockList.List.CONTENT_URI, null, null);
        }
    }
    /**
     * ブロック履歴の簡易編集.
     */
    public static class BlockHistoryItem {
        private static final int MAX_BLOCK_HISTORY = 300;
        public static final void add(Context context, String number, long time) {
            ContentResolver cr = context.getContentResolver();
            // 履歴を追加
            ContentValues values = new ContentValues();
            values.put(BlockList.History.NUMBER, number);
            values.put(BlockList.History.TIME  , time  );
            cr.insert(BlockList.History.CONTENT_URI, values);
            // 履歴の件数が多い場合は削除
            Uri uri = BlockList.History.CONTENT_URI;
            Cursor c = cr.query(uri, null, null, null, null);
            try {
                if (c.moveToPosition(MAX_BLOCK_HISTORY - 1)) {
                    StringBuilder where = new StringBuilder();
                    where.append(BlockList.History.TIME);
                    where.append("<");
                    where.append(c.getLong(c.getColumnIndex(BlockList.History.TIME)));
                    cr.delete(uri, where.toString(), null);
                }
            } finally {
                c.close();
            }
        }
        public static final void delete(Context context, long id) {
            ContentResolver cr = context.getContentResolver();
            Uri uri = ContentUris.withAppendedId(BlockList.History.CONTENT_URI, id);
            cr.delete(uri, null, null);
        }
        public static final void deleteAll(Context context) {
            ContentResolver cr = context.getContentResolver();
            cr.delete(BlockList.History.CONTENT_URI, null, null);
        }
    }
    /**
     * Prefix/Suffixリストの簡易編集.
     */
    public static class EditNumberItem {
        public static final void insert(Context context, String name, String prefix, String suffix, int option) {
            ContentResolver cr = context.getContentResolver();
            ContentValues values = new ContentValues();
            values.put(EditNumber.List.NAME  , name  );
            values.put(EditNumber.List.PREFIX, prefix);
            values.put(EditNumber.List.SUFFIX, suffix);
            values.put(EditNumber.List.OPTION, option);
            cr.insert(EditNumber.List.CONTENT_URI, values);
        }
        public static final void update(Context context, long id, String name, String prefix, String suffix, int option) {
            ContentResolver cr = context.getContentResolver();
            ContentValues values = new ContentValues();
            Uri uri = ContentUris.withAppendedId(EditNumber.List.CONTENT_URI, id);
            values.put(EditNumber.List.NAME  , name  );
            values.put(EditNumber.List.PREFIX, prefix);
            values.put(EditNumber.List.SUFFIX, suffix);
            values.put(EditNumber.List.OPTION, option);
            cr.update(uri, values, null, null);
        }
        public static final void delete(Context context, long id) {
            ContentResolver cr = context.getContentResolver();
            Uri uri = ContentUris.withAppendedId(EditNumber.List.CONTENT_URI, id);
            cr.delete(uri, null, null);
        }
        public static final void deleteAll(Context context) {
            ContentResolver cr = context.getContentResolver();
            cr.delete(EditNumber.List.CONTENT_URI, null, null);
        }
    }
}
