package jp.gr.java_conf.ofnhwx.callconfirm.utility.thread;

import jp.gr.java_conf.ofnhwx.callconfirm.utility.Utility;
import android.content.Context;
import android.os.RemoteException;

import com.android.internal.telephony.ITelephony;

/**
 * 通話を自動で終了させる.
 * @author yuta
 */
public class AutoEndCall extends Thread {

    private Context context;
    private long time;
    private long time30;
    private long time60;
    private boolean last30;
    private boolean last60;

    private AutoEndCall(Context context, int wait) {
        this.context = context;
        this.last30  = wait > 30;
        this.last60  = wait > 60;
        this.time    = System.currentTimeMillis() + (wait * 1000L);
        this.time30  = last30 ? this.time - (30 * 1000L) : 0;
        this.time60  = last60 ? this.time - (60 * 1000L) : 0;
    }

    @Override
    public void run() {
        try {
            ITelephony phone = Utility.connectToTelephonyService(context);
            if (phone == null) {
                return;
            }
            while (true) {
                Thread.sleep(1000);
                long now = System.currentTimeMillis();
                if (!phone.isOffhook() || now > time) {
                    break;
                }
                if (last30 && now > time30) {
                    last30 = false;
                    Utility.vibrate(context);
                }
                if (last60 && now > time60) {
                    last60 = false;
                    Utility.vibrate(context);
                }
            }
            if (phone.isOffhook()) {
                phone.silenceRinger();
                phone.endCall();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public static final void execute(Context context, int wait) {
        new AutoEndCall(context, wait).start();
    }

}
