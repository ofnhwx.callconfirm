package jp.gr.java_conf.ofnhwx.callconfirm.utility.thread;

import jp.gr.java_conf.ofnhwx.callconfirm.utility.DB;
import android.content.Context;

/**
 * ブロックした履歴の追加.
 * @author yuta
 */
public class AddBlockHistory extends Thread {
    private Context context;
    private String  number ;
    private long    time   ;
    private AddBlockHistory(Context context, String number) {
        this.context = context;
        this.number  = number;
        this.time    = System.currentTimeMillis();
    }
    @Override
    public void run() {
        DB.BlockHistoryItem.add  (context, number, time);
        DB.BlockListItem.addCount(context, number, time);
    }
    public static final void execute(Context context, String number) {
        new AddBlockHistory(context, number).start();
    }
}
