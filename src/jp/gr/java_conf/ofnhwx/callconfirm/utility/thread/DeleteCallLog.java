package jp.gr.java_conf.ofnhwx.callconfirm.utility.thread;

import jp.gr.java_conf.ofnhwx.olib.utils.OUtil.NumberType;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;

/**
 * 着信履歴の削除.
 * @author yuta
 */
public class DeleteCallLog extends Thread {

    private Context context;
    private String  number ;

    private DeleteCallLog(Context context, String number) {
        this.context = context;
        this.number  = number ;
    }

    @Override
    public void run() {
        // ----- 通話履歴に登録されるまで時間があるので少し待機 -----
        // エラー発生時にはとりあえず終了する(通話履歴を消せないのは仕方ない)
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return;
        }
        // ----- 通話履歴から最後の着信を取得する -----
        // 取得できない場合は仕方ないから終了する
        LastCall lastCall = LastCall.get(context);
        if (lastCall == null) {
            return;
        }
        // ----- 通話履歴を削除 -----
        // 安全重視で駄目な場合は何もしないようにする
        if ((number == null && (NumberType.get(lastCall.number) != NumberType.NORMAL)) ||
            (number != null && number.equals(lastCall.number))) {
            ContentResolver cr = context.getContentResolver();
            String where = CallLog.Calls._ID + "=" + lastCall.id;
            cr.delete(CallLog.Calls.CONTENT_URI, where, null);
        }
    }

    public static final void execute(Context context, String number) {
        new DeleteCallLog(context, number).start();
    }

}

/**
 * 最後の着信を取得するクラス.
 */
class LastCall {

    public long   id;
    public String number;

    private LastCall(long id, String number) {
        this.id     = id;
        this.number = number;
    }

    public static final LastCall get(Context context) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = CallLog.Calls.CONTENT_URI;
        String sortOrder  = CallLog.Calls.DEFAULT_SORT_ORDER + " LIMIT 1";
        Cursor c = cr.query(uri, null, null, null, sortOrder);
        if (c == null) {
            return null;
        }
        try {
            if (c.moveToFirst()) {
                long   id     = c.getLong  (c.getColumnIndex(CallLog.Calls._ID   ));
                String number = c.getString(c.getColumnIndex(CallLog.Calls.NUMBER));
                return new LastCall(id, number);
            } else {
                return null;
            }
        } finally {
            c.close();
        }
    }

}
