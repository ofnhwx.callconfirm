package jp.gr.java_conf.ofnhwx.callconfirm.utility.thread;

import jp.gr.java_conf.ofnhwx.callconfirm.utility.info.ContactInfo;
import android.graphics.Bitmap;
import android.os.AsyncTask;

/**
 * 写真を非同期で取得するタスク.
 * @author yuta
 */
public class GetPhotoTask extends AsyncTask<Void, Void, Bitmap> {

    public interface GetPhotoCallback {
        public void onGetPhotoCallback(Bitmap photo);
    }

    private ContactInfo info;
    private int index;
    private GetPhotoCallback callback;

    public GetPhotoTask(ContactInfo info, int index, GetPhotoCallback callback) {
        this.info     = info;
        this.index    = index;
        this.callback = callback;
    }

    @Override
    protected Bitmap doInBackground(Void... args) {
        return info.getPhoto(index);
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if (callback != null) {
            callback.onGetPhotoCallback(result);
        }
    }

}
