package jp.gr.java_conf.ofnhwx.callconfirm.utility.thread;

import jp.gr.java_conf.ofnhwx.callconfirm.utility.info.ContactInfo;
import android.content.Context;
import android.os.AsyncTask;

/**
 * 電話帳を非同期で検索するタスク.
 * @author yuta
 */
public class GetContactTask extends AsyncTask<Void, Void, ContactInfo> {

    public interface GetContactCallback {
        public void onGetContactCallback(ContactInfo info);
    }

    private Context context;
    private String number;
    private GetContactCallback callback;

    public GetContactTask(Context fragment, String number, GetContactCallback callback) {
        this.context  = fragment;
        this.number   = number;
        this.callback = callback;
    }

    @Override
    protected ContactInfo doInBackground(Void... args) {
        return new ContactInfo(context, number);
    }

    @Override
    protected void onPostExecute(ContactInfo result) {
        if (callback != null) {
            callback.onGetContactCallback(result);
        }
    }

}
