package jp.gr.java_conf.ofnhwx.callconfirm.utility;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.olib.utils.OPreference;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.text.TextUtils;

/**
 * パラメータの受け渡し用の定数を定義.
 * @author yuta
 */
public abstract class Param {

    //
    public static final int VERSION_CODE_2_0_0 = 51;

    //
    public static final String ID     = "param_id";
    public static final String NAME   = "param_name";
    public static final String NUMBER = "param_number";
    public static final String SCHEME = "param_scheme";

    // 文字・写真サイズの既定値
    private static final float DEFAULT_PICTURE_SIZE = 54.0f;
    private static final float DEFAULT_TITLE_SIZE   = 12.0f;
    private static final float DEFAULT_TEXT_SIZE    = 12.0f;

    // ダイアログに使用する文字色
    private static final int[] COLORS = {
        Color.WHITE,
        Color.LTGRAY,
        Color.GRAY,
        Color.DKGRAY,
        Color.BLACK,
        Color.MAGENTA,
        Color.RED,
        Color.YELLOW,
        Color.GREEN,
        Color.CYAN,
        Color.BLUE
    };

    // テーマの設定
    public static final int THEMA_BLACK  = 0;
    public static final int THEMA_WHITE  = 1;
    public static final int THEMA_NONE   = 2;
    public static final int THEMA_DIALOG = 3;

    // バイブレーションのデフォルトパターン
    private static final String DEFAULT_VIBRATE_PATTERN = " ^ ^";

    // 自動通話切断の時間
    private static final int DEFAULT_AUTO_END_TIME = 300;

    public static final int getMyPictureSize(Context context) {
        float size = getFloat(context, R.string.key_picture_size, DEFAULT_PICTURE_SIZE);
        return (int)(size * getDensity(context));
    }

    public static final int getMyTitleSize(Context context) {
        float size = getFloat(context, R.string.key_title_size, DEFAULT_TITLE_SIZE);
        return (int)(size * getScaledDensity(context));
    }

    public static final int getMyTitleColor(Context context) {
        return COLORS[getInteger(context, R.string.key_title_color, 0)];
    }

    public static final int getMyTextSize(Context context) {
        final float size = getFloat(context, R.string.key_text_size, DEFAULT_TEXT_SIZE);
        return (int)(size * getScaledDensity(context));
    }

    public static final int getMyTextColor(Context context) {
        return COLORS[getInteger(context, R.string.key_text_color, 0)];
    }

    public static final int getMyTheme(Context context) {
        return getInteger(context, R.string.key_theme_color, THEMA_BLACK);
    }

    public static final long[] getVibrationPattern(Context context) {
        // パターン文字列の取得
        OPreference prefs = OPreference.getInstance(context);
        String pattern = prefs.getString(R.string.key_vibrate_pattern, null);
        pattern = TextUtils.isEmpty(pattern) ? DEFAULT_VIBRATE_PATTERN : pattern;
        // パターン文字列からパターン配列を作成
        long length0 =   50;
        long length1 =  200;
        long length2 =  500;
        long length3 = 1000;
        long[] array = new long[pattern.length()];
        for (int i = 0; i < array.length; i++) {
            switch (pattern.charAt(i)) {
            case '^': array[i] = length1; break;
            case '.': array[i] = length2; break;
            case '_': array[i] = length3; break;
            default : array[i] = length0; break;
            }
        }
        return array;
    }

    public static final int getAutoEndTime(Context context) {
        return getInteger(context, R.string.key_auto_reject_time, DEFAULT_AUTO_END_TIME);
    }

    public static final int getVersionCode(Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            return info.versionCode;
        } catch (NameNotFoundException e) {
            return 0;
        }
    }

    public static final int getPrevVersionCode(Context context) {
        return getInteger(context, R.string.key_version, 0);
    }

    private static final float getFloat(Context context, int key, float defValue) {
        OPreference prefs = OPreference.getInstance(context);
        try {
            return Float.parseFloat(prefs.getString(key, Float.toString(defValue)));
        } catch (NumberFormatException e) {
            return defValue;
        }
    }

    private static final int getInteger(Context context, int key, int defValue) {
        OPreference prefs = OPreference.getInstance(context);
        try {
            return Integer.parseInt(prefs.getString(key, Integer.toString(defValue)));
        } catch (NumberFormatException e) {
            return defValue;
        }
    }

    private static final float getDensity(Context context) {
        return context.getResources().getDisplayMetrics().scaledDensity;
    }

    private static final float getScaledDensity(Context context) {
        return context.getResources().getDisplayMetrics().scaledDensity;
    }

}
