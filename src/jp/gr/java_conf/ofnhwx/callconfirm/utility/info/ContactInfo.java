package jp.gr.java_conf.ofnhwx.callconfirm.utility.info;

import java.util.ArrayList;
import java.util.List;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Param;
import jp.gr.java_conf.ofnhwx.olib.compatibility.OContacts;
import jp.gr.java_conf.ofnhwx.olib.utils.OBitmap;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;

/**
 * コンタクトの取り扱いでちょっと楽をするためクラス.
 * @author yuta
 */
public class ContactInfo {

    private static class Contact {
        public final String name ;
        public final long   photo;
        public Contact(String name, long photo) {
            this.name  = name;
            this.photo = photo;
        }
    }

    private Context context;
    private List<Contact> contacts = new ArrayList<ContactInfo.Contact>();
    private boolean isFavorite;

    public ContactInfo(Context context, String number) {
        this.context = context.getApplicationContext();
        Uri uri = Uri.withAppendedPath(OContacts.PHONES_CONTENT_FILTER_URL, Uri.encode(number));
        String[] projection = { OContacts.NAME, OContacts.PHOTO_ID, OContacts.STARRED };
        Cursor c = context.getContentResolver().query(uri, projection, null, null, null);
        if (c == null) {
            isFavorite = false;
            return;
        }
        try {
            int colName  = c.getColumnIndex(OContacts.NAME    );
            int colPhoto = c.getColumnIndex(OContacts.PHOTO_ID);
            int colStar  = c.getColumnIndex(OContacts.STARRED );
            boolean isStarred = false;
            while (c.moveToNext()) {
                contacts.add(new Contact(c.getString(colName), c.getLong(colPhoto)));
                isStarred = isStarred || (c.getInt(colStar) == 1);
            }
            isFavorite = isStarred;
        } finally {
            c.close();
        }
    }

    /**
     * [index]番目の名前を取得.
     * @param index
     * @return
     */
    public String getName(int index) {
        if (isFound()) {
            return contacts.get(index).name;
        } else {
            return context.getString(R.string.callconfirm_unknown);
        }
    }

    /**
     * [index]番目の写真を取得
     * @param index
     * @return
     */
    public Bitmap getPhoto(int index) {
        int size = Param.getMyPictureSize(context);
        if (!isFound()) {
            return OBitmap.decodeResource(context, R.drawable.contact_unknown, size, true);
        }
        // 'photoId'から画像を検索
        Uri uri = ContentUris.withAppendedId(OContacts.PHOTOS_CONTENT_URI, contacts.get(index).photo);
        String[] projection = new String[] { OContacts.PHOTO };
        Cursor c = context.getContentResolver().query(uri, projection, null, null, null);
        if (c == null) {
            return OBitmap.decodeResource(context, R.drawable.contact_unknown, size, true);
        }
        // 画像をバイト配列で取得
        byte[] photoData;
        try {
            if (c.moveToFirst()) {
                photoData = c.getBlob(c.getColumnIndex(OContacts.PHOTO));
            } else {
                photoData = null;
            }
        } finally {
            c.close();
        }
        // バイト配列からBitmap画像を作成
        if (photoData == null) {
            return OBitmap.decodeResource(context, R.drawable.contact_picture, size, true);
        } else {
            return OBitmap.decodeByteArray(photoData, size, true);
        }
    }

    /**
     * 電話番号の一致したコンタクトの件数を取得.
     * @return
     */
    public int getCount() {
        return contacts.size();
    }

    /**
     * コンタクトの登録確認.
     * @return true:登録されている / false:未登録
     */
    public boolean isFound() {
        return contacts.size() > 0;
    }

    /**
     * お気に入りの確認.
     * @return true:お気に入り / false:普通のコンタクト
     */
    public boolean isFavorite() {
        return isFavorite;
    }

}
