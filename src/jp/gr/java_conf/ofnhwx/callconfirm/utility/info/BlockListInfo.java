package jp.gr.java_conf.ofnhwx.callconfirm.utility.info;

import jp.gr.java_conf.ofnhwx.callconfirm.provider.BlockList;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.telephony.PhoneNumberUtils;

/**
 * ブロックリストをいろいろするクラス.
 * @author yuta
 */
public class BlockListInfo {

    private final long   id    ;
    private final String number;
    private final int    count ;
    private final long   time  ;

    public BlockListInfo(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        String number    = PhoneNumberUtils.stripSeparators(phoneNumber);
        String selection = BlockList.List.NUMBER + "='" + (number == null ? "" : number) + "'";
        String sortOrder = BlockList.List.DEFAULT_SORT_ORDER + " LIMIT 1";
        Cursor c = cr.query(BlockList.List.CONTENT_URI, null, selection, null, sortOrder);
        try {
            if (c.moveToFirst()) {
                this.id     = c.getLong  (c.getColumnIndex(BlockList.List._ID));
                this.number = c.getString(c.getColumnIndex(BlockList.List.NUMBER));
                this.count  = c.getInt   (c.getColumnIndex(BlockList.List.COUNT));
                this.time   = c.getLong  (c.getColumnIndex(BlockList.List.TIME));
            } else {
                this.id     = 0;
                this.number = null;
                this.count  = 0;
                this.time   = 0;
            }
        } finally {
            c.close();
        }
    }

    /**
     * IDを取得.
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     * 電話番号を取得.
     * @return
     */
    public String getNumber() {
        return number;
    }

    /**
     * 着信を拒否した回数を取得.
     * @return
     */
    public int getCount() {
        return count;
    }

    /**
     * 最後に着信を拒否した時間を取得.
     * @return
     */
    public long getTime() {
        return time;
    }

    /**
     * ブロックリストに登録されているかの確認.
     * @return true:登録されている / false:登録されていない
     */
    public boolean isBlockNumber() {
        return number != null;
    }

}
