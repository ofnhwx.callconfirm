package jp.gr.java_conf.ofnhwx.callconfirm.utility.info;

import jp.gr.java_conf.ofnhwx.callconfirm.provider.AllowList;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.telephony.PhoneNumberUtils;

/**
 * 許可リストでいろいろするクラス.
 * @author yuta
 */
public class AllowListInfo {

    private final long   id    ;
    private final String name  ;
    private final String number;

    public AllowListInfo(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        String number    = PhoneNumberUtils.stripSeparators(phoneNumber);
        String selection = AllowList.List.NUMBER + "='" + (number == null ? "" : number) + "'";
        String sortOrder = AllowList.List.DEFAULT_SORT_ORDER + " LIMIT 1";
        Cursor c = cr.query(AllowList.List.CONTENT_URI, null, selection, null, sortOrder);
        try {
            if (c.moveToFirst()) {
                this.id     = c.getLong  (c.getColumnIndex(AllowList.List._ID   ));
                this.name   = c.getString(c.getColumnIndex(AllowList.List.NAME  ));
                this.number = c.getString(c.getColumnIndex(AllowList.List.NUMBER));
            } else {
                this.id     = 0;
                this.name   = null;
                this.number = null;
            }
        } finally {
            c.close();
        }
    }

    /**
     * IDを取得.
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     * 名称を取得.
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * 電話番号を取得.
     * @return
     */
    public String getNumber() {
        return number;
    }

    /**
     * 許可リストに登録されているかの確認.
     * @return true:登録されている / false:登録されていない
     */
    public boolean isAllowNumber() {
        return number != null;
    }

}
