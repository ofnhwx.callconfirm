package jp.gr.java_conf.ofnhwx.callconfirm.activity;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm.DialerActivity;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm.MissedCallActivity;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.BaseActivity;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Param;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Utility;
import jp.gr.java_conf.ofnhwx.olib.ErrorReporter;
import jp.gr.java_conf.ofnhwx.olib.base.BasePreferenceActivity;
import jp.gr.java_conf.ofnhwx.olib.utils.OPreference;
import jp.gr.java_conf.ofnhwx.olib.utils.OUtil;
import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceManager;

/**
 * 設定画面です.
 * @author yuta
 */
public class MainPreference extends BasePreferenceActivity
        implements OnSharedPreferenceChangeListener {

    // エラー報告ダイアログのID
    private static final int DIALOG_ID_ERROR_REPORT = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ErrorReporter.initialize(this);
        addPreferencesFromResource(R.xml.main);
        OPreference prefs = OPreference.getInstance(this);
        // バージョン確認いろいろ
        int old = Param.getPrevVersionCode(this);
        int now = Param.getVersionCode(this);
        if (old < Param.VERSION_CODE_2_0_0) {
            PreferenceManager pm = getPreferenceManager();
            SharedPreferences preferences = pm.getSharedPreferences();
            Editor editor = preferences.edit();
            editor.clear();
            editor.commit();
        }
        if (old < now) {
            prefs.write(R.string.key_version, Integer.toString(now));
        }
        // ACTION_DIAL、不在着信通知に反応するための設定を取得
        setChecked(R.string.key_dial  , OUtil.getComponetnEnabled(this, DialerActivity.class    ));
        setChecked(R.string.key_missed, OUtil.getComponetnEnabled(this, MissedCallActivity.class));
        //
        findPreference(R.string.key_use_toast).setEnabled(!prefs.getBoolean(R.string.key_use_service, false));
        // android 2.3 (Gingerbread) 以降への対応
        Preference incoming = findPreference(R.string.category_incoming_call);
        incoming.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Utility.showGingerbreadUsersSecurityPopup(MainPreference.this);
                return false;
            }
        });
        // Vibrationのテスト
        Preference vibration = findPreference(R.string.key_vibrate_test);
        vibration.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Utility.vibrate(MainPreference.this);
                return true;
            }
        });
        // 旧バージョンのダウンロード
        Preference download = findPreference(R.string.key_download_old_version);
        download.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Uri uri = Uri.parse("http://dl.dropbox.com/u/883777/callconfirm_1.4.2.apk");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                return true;
            }
        });
        // 動かない機能は無効にしてもいいよね？
        if (!OUtil.hasPermission(this, Manifest.permission.MODIFY_PHONE_STATE)) {
            // 通話の自動終了
            prefs.write(R.string.key_auto_reject, false);
            findPreference(R.string.key_auto_reject).setEnabled(false);
        }
        // バグ報告
        if (savedInstanceState == null) {
            ErrorReporter.show(this, DIALOG_ID_ERROR_REPORT);
        }
        // 終了予約
        BaseActivity.addFinishOrder(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BaseActivity.removeFinishOrder(this);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DIALOG_ID_ERROR_REPORT:
            return ErrorReporter.getDialog(this, R.drawable.callconfirm);
        }
        return null;
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        changeComponentEnabled(key, R.string.key_dial  , DialerActivity.class    );
        changeComponentEnabled(key, R.string.key_missed, MissedCallActivity.class);
        //
        OPreference prefs = OPreference.getInstance(this);
        if (key.equals(getString(R.string.key_use_service))) {
            if (prefs.getBoolean(key, false)) {
                setChecked(R.string.key_use_dialog, false);
                findPreference(R.string.key_use_toast).setEnabled(false);
            } else {
                findPreference(R.string.key_use_toast).setEnabled(true);
            }
        }
        if (key.equals(getString(R.string.key_use_dialog))) {
            if (prefs.getBoolean(key, false)) {
                setChecked(R.string.key_use_service, false);
            }
        }
    }

    private void changeComponentEnabled(String key, int targetKey, Class<?> clazz) {
        if (key.equals(getString(targetKey))) {
            OPreference prefs = OPreference.getInstance(this);
            boolean enabled = prefs.getBoolean(key, false);
            OUtil.setComponentEnabled(this, clazz, enabled);
        }
    }

}
