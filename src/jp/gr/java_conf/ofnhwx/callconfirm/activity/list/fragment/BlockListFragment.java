package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.BlockHistoryActivity;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.adapter.BlockListAdapter;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog.BlockListEditDialog;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog.BlockListRemoveAllDialog;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog.BlockListRemoveDialog;
import jp.gr.java_conf.ofnhwx.callconfirm.provider.BlockList;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.DB;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Utility;
import jp.gr.java_conf.ofnhwx.olib.base.BaseListFragmentC;
import jp.gr.java_conf.ofnhwx.olib.compatibility.OCompatibility;
import jp.gr.java_conf.ofnhwx.olib.compatibility.OContacts;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CallLog;
import android.support.v4.app.ListFragment;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuCompat;
import android.support.v4.widget.CursorAdapter;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

/**
 * Prefix/Suffixを操作する{@link ListFragment}
 * @author yuta
 */
public class BlockListFragment extends BaseListFragmentC {

    private static final int REQUEST_PICK_PHONES = 0;

    @Override
    protected CursorAdapter onCreateAdapter() {
        return new BlockListAdapter(getActivity());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setEmptyText(R.string.list_empty);
        getLoaderManager().initLoader(0, null, this);
        // メニューの設定
        setHasOptionsMenu(true);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
        case REQUEST_PICK_PHONES:
            new Handler().post(new Runnable() {
                public void run() {
                    BlockListEditDialog.show(getActivity(), 0, Utility.getNumberFromIntent(getActivity(), data));
                }
            });
            break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.option_block_list, menu);
        MenuCompat.setShowAsAction(menu.findItem(R.id.option_add), OCompatibility.MenuItem.SHOW_AS_ACTION_IF_ROOM);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.option_remove_all).setEnabled(mAdapter.getCount() > 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.option_add_normal:
            BlockListEditDialog.show(getActivity(), 0, null);
            break;
        case R.id.option_add_last:
            ContentResolver cr = getActivity().getContentResolver();
            Uri uri = CallLog.Calls.CONTENT_URI;
            String selection = CallLog.Calls.TYPE + ">" + CallLog.Calls.INCOMING_TYPE;
            String sortOrder = CallLog.Calls.DEFAULT_SORT_ORDER + " limit 1";
            Cursor c = cr.query(uri, null, selection, null, sortOrder);
            if (c == null) {
                break;
            }
            try {
                if (c.moveToFirst()) {
                    BlockListEditDialog.show(getActivity(), 0, c.getString(c.getColumnIndex(CallLog.Calls.NUMBER)));
                }
            } finally {
                c.close();
            }
            break;
        case R.id.option_add_contact:
            Intent intent = new Intent(Intent.ACTION_PICK, OContacts.PHONES_CONTENT_URI);
            startActivityForResult(intent, REQUEST_PICK_PHONES);
            break;
        case R.id.option_remove_all:
            BlockListRemoveAllDialog.show(getActivity());
            break;
        }
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_block_list, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Cursor c = mAdapter.getCursor();
        long   id     = c.getLong  (c.getColumnIndex(BlockList.List._ID   ));
        String number = c.getString(c.getColumnIndex(BlockList.List.NUMBER));
        long   time   = c.getLong  (c.getColumnIndex(BlockList.List.TIME  ));
        switch (item.getItemId()) {
        case R.id.context_open:
            Intent intent = new Intent(getActivity(), BlockHistoryActivity.class);
            intent.putExtra(Intent.EXTRA_PHONE_NUMBER, number);
            startActivity(intent);
            break;
        case R.id.context_edit:
            BlockListEditDialog.show(getActivity(), id, null);
            break;
        case R.id.context_remove:
            BlockListRemoveDialog.show(getActivity(), id);
            break;
        case R.id.context_reset:
            DB.BlockListItem.update(getActivity(), id, number, time, 0);
            break;
        }
        return true;
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = BlockList.List.CONTENT_URI;
        String sortOrder = BlockList.List.DEFAULT_SORT_ORDER;
        return new CursorLoader(getActivity(), uri, null, null, null, sortOrder);
    }

}
