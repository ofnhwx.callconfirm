package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.provider.BlockList;
import jp.gr.java_conf.ofnhwx.olib.base.BaseCursorAdapter;
import android.content.Context;
import android.database.Cursor;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.widget.TextView;

/**
 * アイテムの表示に使用するアダプタ.
 * @author yuta
 */
public class BlockHistoryAdapter extends BaseCursorAdapter {

    private static final SimpleDateFormat sdf = new SimpleDateFormat();

    private int colNumber;
    private int colTime  ;

    public BlockHistoryAdapter(Context context) {
        super(context, R.layout.item_blocklist,
                new String[] { BlockList.History.NUMBER, BlockList.History.TIME },
                new int   [] { R.id.list_number        , R.id.list_time         },
                0);
        setViewBinder(this);
    }

    @Override
    protected void onSetColumns(Cursor c) {
        colNumber = c.getColumnIndex(BlockList.History.NUMBER);
        colTime   = c.getColumnIndex(BlockList.History.TIME  );
    }

    public boolean setViewValue(View v, Cursor c, int col) {
        if (col == colNumber) {
            String number = c.getString(col);
            if (number == null) {
                ((TextView)v).setText(R.string.unknown_number);
            } else {
                ((TextView)v).setText(PhoneNumberUtils.formatNumber(number));
            }
            return true;
        }
        if (col == colTime) {
            ((TextView)v).setText(sdf.format(new Date(c.getLong(col))));
            return true;
        }
        return false;
    }

}
