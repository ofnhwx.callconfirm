package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog;

import jp.gr.java_conf.ofnhwx.callconfirm.utility.DB;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;

/**
 * Prefix/Suffixリストの削除.
 * @author yuta
 */
public class EditNumberRemoveAllDialog extends BaseRemoveConfirmDialog {

    public void onClick(DialogInterface dialog, int which) {
        DB.EditNumberItem.deleteAll(getActivity());
    }

    public static final void show(FragmentActivity activity) {
        DialogFragment fragment = new EditNumberRemoveAllDialog();
        fragment.show(activity.getSupportFragmentManager(), EditNumberRemoveAllDialog.class.toString());
    }

}
