package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog;

import jp.gr.java_conf.ofnhwx.callconfirm.provider.BlockList;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.DB;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Param;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.info.BlockListInfo;
import jp.gr.java_conf.ofnhwx.olib.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

/**
 * ブロックリストの追加・編集.
 * @author yuta
 */
public class BlockListEditDialog extends DialogFragment implements OnClickListener {

    private EditText numberView;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_blocklist, null, false);
        numberView = (EditText)view.findViewById(R.id.block_list_number);
        numberView.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        view.findViewById(R.id.positive).setOnClickListener(this);
        view.findViewById(R.id.negative).setOnClickListener(this);
        //
        long id = getArguments().getLong(Param.ID);
        if (id == 0) {
            numberView.setText(getArguments().getString(Intent.EXTRA_PHONE_NUMBER));
        } else {
            Uri uri = ContentUris.withAppendedId(BlockList.List.CONTENT_URI, id);
            Cursor c = getActivity().getContentResolver().query(uri, null, null, null, null);
            try {
                c.moveToFirst();
                numberView.setText(c.getString(c.getColumnIndex(BlockList.List.NUMBER)));
            } finally {
                c.close();
            }
        }
        //
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.add_blocklist_dialog_title);
        builder.setView(view);
        builder.setCancelable(true);
        return builder.create();
    }

    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.positive:
            long id = getArguments().getLong(Param.ID);
            String number = numberView.getText().toString();
            //
            BlockListInfo info = new BlockListInfo(getActivity(), number);
            if (info.isBlockNumber() && id != info.getId()) {
                numberView.setError(getText(R.string.already_registered));
                numberView.requestFocus();
                return;
            }
            //
            if (id == 0) {
                DB.BlockListItem.insert(getActivity(), number);
            } else {
                DB.BlockListItem.update(getActivity(), id, number, System.currentTimeMillis(),  0);
            }
            break;
        }
        dismiss();
    }

    public static final void show(FragmentActivity activity, long id, String number) {
        DialogFragment fragment = new BlockListEditDialog();
        Bundle args = new Bundle();
        args.putLong(Param.ID, id);
        args.putString(Intent.EXTRA_PHONE_NUMBER, number);
        fragment.setArguments(args);
        fragment.show(activity.getSupportFragmentManager(), BlockListEditDialog.class.toString());
    }

}
