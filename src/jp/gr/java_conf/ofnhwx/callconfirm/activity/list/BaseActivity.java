package jp.gr.java_conf.ofnhwx.callconfirm.activity.list;

import java.util.HashMap;
import java.util.Map;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.olib.ErrorReporter;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

/**
 * アクティビティの共通部分をまとめたクラス.
 * @author yuta
 */
public abstract class BaseActivity extends FragmentActivity {

    /**
     * 終了を予約している{@link Activity}.
     * <pre>
     * TODO:根本的な解決策が見つかるまでの暫定的な対応
     * </pre>
     */
    private static Map<String, Activity> sActivityMap = new HashMap<String, Activity>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ErrorReporter.initialize(this);
        setContentView(R.layout.base);
        BaseActivity.addFinishOrder(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BaseActivity.removeFinishOrder(this);
    }

    /**
     * {@link Activity}の終了を予約する.
     * @param activity
     */
    public static final void addFinishOrder(Activity activity) {
        sActivityMap.put(activity.toString(), activity);
    }

    /**
     * {@link Activity}の終了の予約を解除する
     * @param activity
     */
    public static final void removeFinishOrder(Activity activity) {
        sActivityMap.remove(activity.toString());
    }

    /**
     * 終了を予約している{@link Activity}を終了させる.
     */
    public static final void finishOrder() {
        try {
            for (Activity activity : sActivityMap.values()) {
                activity.finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sActivityMap.clear();
        }
    }

    /**
     * {@link Fragment}の入替えを行う.
     * @param fragment
     * @return
     * @see #replaceFragment(int, Fragment, boolean)
     */
    protected int replaceFragment(Fragment fragment) {
        return replaceFragment(fragment, true);
    }

    /**
     * {@link Fragment}の入替えを行う.
     * @param fragment
     * @param canPopBack
     * @return
     * @see #replaceFragment(int, Fragment, boolean)
     */
    protected int replaceFragment(Fragment fragment, boolean canPopBack) {
        return replaceFragment(R.id.fragment, fragment, canPopBack);
    }

    /**
     * {@link Fragment}の入替えを行う.
     * @param id 対象となる{@link View}の<code>id</code>
     * @param fragment 新しい{@link Fragment}
     * @param canPopBack <code>true</code>ならトランザクションをスタックに追加する
     * @return {@link FragmentTransaction#commit()}の結果
     */
    protected int replaceFragment(int id, Fragment fragment, boolean canPopBack) {
        FragmentTransaction tr = getSupportFragmentManager().beginTransaction();
        tr.replace(id, fragment);
        tr.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        if (canPopBack) {
            tr.addToBackStack(null);
        }
        return tr.commit();
    }

}
