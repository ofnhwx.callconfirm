package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog;

import jp.gr.java_conf.ofnhwx.callconfirm.provider.AllowList;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.DB;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Param;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.info.AllowListInfo;
import jp.gr.java_conf.ofnhwx.olib.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

/**
 * 許可リストの追加・編集.
 * @author yuta
 */
public class AllowListEditDialog extends DialogFragment implements OnClickListener {

    private TextView nameView;
    private TextView numberView;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_allowlist, null, false);
        nameView   = (TextView)view.findViewById(R.id.allow_list_name  );
        numberView = (TextView)view.findViewById(R.id.allow_list_number);
        numberView.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        view.findViewById(R.id.positive).setOnClickListener(this);
        view.findViewById(R.id.negative).setOnClickListener(this);
        //
        long id = getArguments().getLong(Param.ID);
        if (id == 0) {
            numberView.setText(getArguments().getString(Intent.EXTRA_PHONE_NUMBER));
        } else {
            Uri uri = ContentUris.withAppendedId(AllowList.List.CONTENT_URI, id);
            Cursor c = getActivity().getContentResolver().query(uri, null, null, null, null);
            try {
                c.moveToFirst();
                nameView  .setText(c.getString(c.getColumnIndex(AllowList.List.NAME  )));
                numberView.setText(c.getString(c.getColumnIndex(AllowList.List.NUMBER)));
            } finally {
                c.close();
            }
        }
        //
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.add_allowlist_dialog_title);
        builder.setView(view);
        builder.setCancelable(true);
        return builder.create();
    }

    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.positive:
            long id = getArguments().getLong(Param.ID);
            String name   = nameView  .getText().toString();
            String number = numberView.getText().toString();
            //
            AllowListInfo info = new AllowListInfo(getActivity(), number);
            if (info.isAllowNumber() && id != info.getId()) {
                numberView.setError(getText(R.string.already_registered));
                numberView.requestFocus();
                return;
            }
            //
            if (id == 0) {
                DB.AllowListItem.insert(getActivity(), name, number);
            } else {
                DB.AllowListItem.update(getActivity(), id, name, number);
            }
            break;
        }
        dismiss();
    }

    public static final void show(FragmentActivity activity, long id, String number) {
        DialogFragment fragment = new AllowListEditDialog();
        Bundle args = new Bundle();
        args.putLong(Param.ID, id);
        args.putString(Intent.EXTRA_PHONE_NUMBER, number);
        fragment.setArguments(args);
        fragment.show(activity.getSupportFragmentManager(), AllowListEditDialog.class.toString());
    }

}
