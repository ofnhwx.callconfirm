package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.provider.BlockList;
import jp.gr.java_conf.ofnhwx.olib.base.BaseCursorAdapter;
import android.content.Context;
import android.database.Cursor;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.widget.TextView;

/**
 * アイテムの表示に使用するアダプタ.
 * @author yuta
 */
public class BlockListAdapter extends BaseCursorAdapter {

    private static final SimpleDateFormat sdf = new SimpleDateFormat();

    private String countFormat;
    private String timeFormat ;

    private int colNumber;
    private int colTime  ;
    private int colCount ;

    public BlockListAdapter(Context context) {
        super(context, R.layout.item_blocklist,
                new String[] { BlockList.List.NUMBER, BlockList.List.COUNT, BlockList.List.TIME },
                new int   [] { R.id.list_number     , R.id.list_count     , R.id.list_time      },
                0);
        countFormat = context.getString(R.string.blocklist_list_count);
        timeFormat  = context.getString(R.string.blocklist_list_time );
        setViewBinder(this);
    }

    @Override
    protected void onSetColumns(Cursor c) {
        colNumber = c.getColumnIndex(BlockList.List.NUMBER);
        colTime   = c.getColumnIndex(BlockList.List.TIME  );
        colCount  = c.getColumnIndex(BlockList.List.COUNT );
    }

    public boolean setViewValue(View v, Cursor c, int col) {
        if (col == colNumber) {
            String number = c.getString(col);
            if (number.length() == 0) {
                ((TextView)v).setText(R.string.unknown_number);
            } else {
                ((TextView)v).setText(PhoneNumberUtils.formatNumber(number));
            }
            return true;
        }
        if (col == colCount) {
            ((TextView)v).setText(String.format(countFormat, c.getLong(col)));
            return true;
        }
        if (col == colTime) {
            long time = c.getLong(col);
            if (time == 0) {
                ((TextView)v).setText(null);
            } else {
                ((TextView)v).setText(String.format(timeFormat, sdf.format(new Date(time))));
            }
            return true;
        }
        return false;
    }

}
