package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.adapter;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.provider.AllowList;
import jp.gr.java_conf.ofnhwx.olib.base.BaseCursorAdapter;
import android.content.Context;
import android.database.Cursor;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.widget.TextView;

/**
 * アイテムの表示に使用するアダプタ.
 * @author yuta
 */
public class AllowListAdapter extends BaseCursorAdapter {

    private int colName  ;
    private int colNumber;

    public AllowListAdapter(Context context) {
        super(context, R.layout.item_allowlist,
                new String[] { AllowList.List.NAME, AllowList.List.NUMBER },
                new int   [] { R.id.list_name     , R.id.list_number      },
                0);
        setViewBinder(this);
    }

    @Override
    protected void onSetColumns(Cursor c) {
        colName   = c.getColumnIndex(AllowList.List.NAME  );
        colNumber = c.getColumnIndex(AllowList.List.NUMBER);
    }

    public boolean setViewValue(View v, Cursor c, int col) {
        if (col == colName) {
            ((TextView)v).setText(c.getString(col));
            return true;
        }
        if (col == colNumber) {
            ((TextView)v).setText(PhoneNumberUtils.formatNumber(c.getString(col)));
            return true;
        }
        return false;
    }

}
