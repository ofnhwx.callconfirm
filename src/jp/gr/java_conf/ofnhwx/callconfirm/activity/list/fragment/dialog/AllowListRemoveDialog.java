package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog;

import jp.gr.java_conf.ofnhwx.callconfirm.utility.DB;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Param;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;

/**
 * 許可リストの削除.
 * @author yuta
 */
public final class AllowListRemoveDialog extends BaseRemoveConfirmDialog {

    public void onClick(final DialogInterface dialog, final int which) {
        final long id = getArguments().getLong(Param.ID);
        DB.AllowListItem.delete(getActivity(), id);
    }

    public static final void show(final FragmentActivity activity, final long id) {
        final DialogFragment fragment = new AllowListRemoveDialog();
        final Bundle args = new Bundle();
        args.putLong(Param.ID, id);
        fragment.setArguments(args);
        fragment.show(activity.getSupportFragmentManager(), AllowListRemoveDialog.class.toString());
    }

}
