package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.adapter.BlockHistoryAdapter;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog.BlockHistoryRemoveAllDialog;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog.BlockHistoryRemoveDialog;
import jp.gr.java_conf.ofnhwx.callconfirm.provider.BlockList;
import jp.gr.java_conf.ofnhwx.olib.base.BaseListFragmentC;
import jp.gr.java_conf.ofnhwx.olib.compatibility.OCompatibility;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuCompat;
import android.support.v4.widget.CursorAdapter;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

/**
 * Prefix/Suffixを操作する{@link ListFragment}
 * @author yuta
 */
public class BlockHistoryFragment extends BaseListFragmentC {

    public static BlockHistoryFragment newInstance(String number) {
        BlockHistoryFragment fragment = new BlockHistoryFragment();
        Bundle args = new Bundle();
        args.putString(Intent.EXTRA_PHONE_NUMBER, number);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected CursorAdapter onCreateAdapter() {
        return new BlockHistoryAdapter(getActivity());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setEmptyText(R.string.list_empty);
        getLoaderManager().initLoader(0, null, this);
        // メニューの設定
        setHasOptionsMenu(true);
        setHasContextMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.option_block_history, menu);
        MenuCompat.setShowAsAction(menu.findItem(R.id.option_add), OCompatibility.MenuItem.SHOW_AS_ACTION_IF_ROOM);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.option_remove_all).setEnabled(mAdapter.getCount() > 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.option_remove_all:
            BlockHistoryRemoveAllDialog.show(getActivity());
            break;
        }
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_block_history, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Cursor c = mAdapter.getCursor();
        long id = c.getLong(c.getColumnIndex(BlockList.List._ID));
        switch (item.getItemId()) {
        case R.id.context_remove:
            BlockHistoryRemoveDialog.show(getActivity(), id);
            break;
        }
        return true;
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = BlockList.History.CONTENT_URI;
        String sortOrder = BlockList.History.DEFAULT_SORT_ORDER;
        return new CursorLoader(getActivity(), uri, null, null, null, sortOrder);
    }

}
