package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Param;
import jp.gr.java_conf.ofnhwx.olib.compatibility.ODialog;
import android.app.Dialog;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * 確認を行うダイアログの共通処理.
 * @author yuta
 */
public abstract class BaseRemoveConfirmDialog extends DialogFragment implements OnClickListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        CharSequence title   = args == null ? null : args.getString(Param.NAME);
        CharSequence message = getText(R.string.dialog_remove_confirm);
        Dialog dialog = ODialog.showOkCancel(getActivity(), title, message, this, null);
        dialog.setCancelable(true);
        return dialog;
    }

}
