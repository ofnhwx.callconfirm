package jp.gr.java_conf.ofnhwx.callconfirm.activity.list;

import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.AllowListFragment;
import android.os.Bundle;

/**
 * 許可リスト - 登録されている番号への発信は確認を行わない.
 * @author yuta
 */
public class AllowListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            replaceFragment(new AllowListFragment(), false);
        }
    }

}
