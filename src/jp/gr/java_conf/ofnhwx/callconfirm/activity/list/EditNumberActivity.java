package jp.gr.java_conf.ofnhwx.callconfirm.activity.list;

import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.EditNumberFragment;
import android.os.Bundle;

/**
 * Prefix/Suffixリスト - このリストを利用して発信前に番号を編集する.
 * @author yuta
 */
public class EditNumberActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            replaceFragment(new EditNumberFragment(), false);
        }
    }

}
