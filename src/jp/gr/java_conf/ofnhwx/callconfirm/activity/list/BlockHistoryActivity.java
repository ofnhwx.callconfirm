package jp.gr.java_conf.ofnhwx.callconfirm.activity.list;

import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.BlockHistoryFragment;
import android.content.Intent;
import android.os.Bundle;

/**
 * ブロック履歴 - 着信をブロックした履歴を記録.
 * @author yuta
 */
public class BlockHistoryActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            String number = getIntent().getStringExtra(Intent.EXTRA_PHONE_NUMBER);
            replaceFragment(BlockHistoryFragment.newInstance(number), false);
        }
    }

}
