package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog;

import jp.gr.java_conf.ofnhwx.callconfirm.utility.DB;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;

/**
 * ブロック履歴の削除.
 * @author yuta
 */
public class BlockHistoryRemoveAllDialog extends BaseRemoveConfirmDialog {

    public void onClick(DialogInterface dialog, int which) {
        DB.BlockHistoryItem.deleteAll(getActivity());
    }

    public static final void show(FragmentActivity activity) {
        DialogFragment fragment = new BlockHistoryRemoveAllDialog();
        fragment.show(activity.getSupportFragmentManager(), BlockHistoryRemoveAllDialog.class.toString());
    }

}
