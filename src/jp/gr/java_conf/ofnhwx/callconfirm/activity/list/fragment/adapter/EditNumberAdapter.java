package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.adapter;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.provider.EditNumber;
import jp.gr.java_conf.ofnhwx.olib.base.BaseCursorAdapter;
import android.content.Context;
import android.database.Cursor;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.TextView;

/**
 * アイテムの表示に使用するアダプタ.
 * @author yuta
 */
public class EditNumberAdapter extends BaseCursorAdapter {

    private int colName;
    private int colPrefix;
    private int colSuffix;
    private int colOpt;

    // 電話番号
    private static final String DEFAULT_NUMBER = "0123456789";
    private String number;
    private boolean flag;

    public EditNumberAdapter(Context context, String number) {
        super(context, R.layout.item_editnumber,
            new String[] { EditNumber.List.NAME, EditNumber.List.PREFIX, EditNumber.List.SUFFIX, EditNumber.List.OPTION },
            new int   [] { R.id.list_name      , R.id.list_prefix      , R.id.list_suffix      , R.id.list_number       },
            0);
        // 電話番号の取得
        this.number = PhoneNumberUtils.formatNumber(number == null ? DEFAULT_NUMBER : number);
        this.flag = this.number.startsWith("0");
        //
        setViewBinder(this);
    }

    public EditNumberAdapter(Context context) {
        this(context, ((TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE)).getLine1Number());
    }

    @Override
    protected void onSetColumns(Cursor c) {
        colName   = c.getColumnIndex(EditNumber.List.NAME  );
        colPrefix = c.getColumnIndex(EditNumber.List.PREFIX);
        colSuffix = c.getColumnIndex(EditNumber.List.SUFFIX);
        colOpt    = c.getColumnIndex(EditNumber.List.OPTION);
    }

    public boolean setViewValue(View v, Cursor c, int col) {
        if (col == colName || col == colPrefix || col == colSuffix) {
            ((TextView)v).setText(c.getString(col));
            return true;
        }
        if (col == colOpt) {
            if (flag && c.getInt(col) != 0) {
                ((TextView)v).setText(number.substring(1));
            } else {
                ((TextView)v).setText(number);
            }
            return true;
        }
        return false;
    }

}
