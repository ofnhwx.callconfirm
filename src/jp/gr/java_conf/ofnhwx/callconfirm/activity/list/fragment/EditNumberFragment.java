package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.adapter.EditNumberAdapter;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog.EditNumberEditDialog;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog.EditNumberRemoveAllDialog;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog.EditNumberRemoveDialog;
import jp.gr.java_conf.ofnhwx.callconfirm.provider.EditNumber;
import jp.gr.java_conf.ofnhwx.olib.base.BaseListFragmentC;
import jp.gr.java_conf.ofnhwx.olib.compatibility.OCompatibility;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuCompat;
import android.support.v4.widget.CursorAdapter;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

/**
 * Prefix/Suffixを操作する{@link ListFragment}
 * @author yuta
 */
public class EditNumberFragment extends BaseListFragmentC {

    @Override
    protected CursorAdapter onCreateAdapter() {
        return new EditNumberAdapter(getActivity());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setEmptyText(R.string.list_empty);
        getLoaderManager().initLoader(0, null, this);
        // メニューの設定
        setHasOptionsMenu(true);
        setHasContextMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.option_edit_number, menu);
        MenuCompat.setShowAsAction(menu.findItem(R.id.option_add), OCompatibility.MenuItem.SHOW_AS_ACTION_IF_ROOM);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.option_remove_all).setEnabled(mAdapter.getCount() > 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.option_add:
            EditNumberEditDialog.show(getActivity(), 0);
            break;
        case R.id.option_remove_all:
            EditNumberRemoveAllDialog.show(getActivity());
            break;
        }
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_edit_remove, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Cursor c = mAdapter.getCursor();
        long id = c.getLong(c.getColumnIndex(EditNumber.List._ID));
        switch (item.getItemId()) {
        case R.id.context_edit:
            EditNumberEditDialog.show(getActivity(), id);
            break;
        case R.id.context_remove:
            EditNumberRemoveDialog.show(getActivity(), id);
            break;
        }
        return true;
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = EditNumber.List.CONTENT_URI;
        String sortOrder = EditNumber.List.DEFAULT_SORT_ORDER;
        return new CursorLoader(getActivity(), uri, null, null, null, sortOrder);
    }

}
