package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.adapter.AllowListAdapter;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog.AllowListEditDialog;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog.AllowListRemoveAllDialog;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog.AllowListRemoveDialog;
import jp.gr.java_conf.ofnhwx.callconfirm.provider.AllowList;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Utility;
import jp.gr.java_conf.ofnhwx.olib.base.BaseListFragmentC;
import jp.gr.java_conf.ofnhwx.olib.compatibility.OCompatibility;
import jp.gr.java_conf.ofnhwx.olib.compatibility.OContacts;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CallLog;
import android.support.v4.app.ListFragment;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuCompat;
import android.support.v4.widget.CursorAdapter;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

/**
 * 許可リストを操作する{@link ListFragment}
 * @author yuta
 */
public class AllowListFragment extends BaseListFragmentC {

    private static final int REQUEST_PICK_PHONES = 0;

    @Override
    protected CursorAdapter onCreateAdapter() {
        return new AllowListAdapter(getActivity());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setEmptyText(R.string.list_empty);
        getLoaderManager().initLoader(0, null, this);
        // メニューの設定
        setHasOptionsMenu(true);
        setHasContextMenu(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
        case REQUEST_PICK_PHONES:
            new Handler().post(new Runnable() {
                public void run() {
                    AllowListEditDialog.show(getActivity(), 0, Utility.getNumberFromIntent(getActivity(), data));
                }
            });
            break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.option_allow_list, menu);
        MenuCompat.setShowAsAction(menu.findItem(R.id.option_add), OCompatibility.MenuItem.SHOW_AS_ACTION_IF_ROOM);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.option_remove_all).setEnabled(mAdapter.getCount() > 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.option_add_normal:
            AllowListEditDialog.show(getActivity(), 0, null);
            break;
        case R.id.option_add_last:
            ContentResolver cr = getActivity().getContentResolver();
            Uri uri = CallLog.Calls.CONTENT_URI;
            String selection = CallLog.Calls.TYPE + "=" + CallLog.Calls.INCOMING_TYPE;
            String sortOrder = CallLog.Calls.DEFAULT_SORT_ORDER + " limit 1";
            Cursor c = cr.query(uri, null, selection, null, sortOrder);
            if (c == null) {
                break;
            }
            try {
                if (c.moveToFirst()) {
                    AllowListEditDialog.show(getActivity(), 0, c.getString(c.getColumnIndex(CallLog.Calls.NUMBER)));
                }
            } finally {
                c.close();
            }
            break;
        case R.id.option_add_contact:
            Intent intent = new Intent(Intent.ACTION_PICK, OContacts.PHONES_CONTENT_URI);
            startActivityForResult(intent, REQUEST_PICK_PHONES);
            break;
        case R.id.option_remove_all:
            AllowListRemoveAllDialog.show(getActivity());
            break;
        }
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_edit_remove, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Cursor c = mAdapter.getCursor();
        long id = c.getLong(c.getColumnIndex(AllowList.List._ID));
        switch (item.getItemId()) {
        case R.id.context_edit:
            AllowListEditDialog.show(getActivity(), id, null);
            break;
        case R.id.context_remove:
            AllowListRemoveDialog.show(getActivity(), id);
            break;
        }
        return true;
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = AllowList.List.CONTENT_URI;
        String sortOrder = AllowList.List.DEFAULT_SORT_ORDER;
        return new CursorLoader(getActivity(), uri, null, null, null, sortOrder);
    }

}
