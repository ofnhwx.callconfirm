package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog;

import jp.gr.java_conf.ofnhwx.callconfirm.utility.DB;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;

/**
 * 許可リストの削除.
 * @author yuta
 */
public class AllowListRemoveAllDialog extends BaseRemoveConfirmDialog {

    public void onClick(DialogInterface dialog, int which) {
        DB.AllowListItem.deleteAll(getActivity());
    }

    public static final void show(FragmentActivity activity) {
        DialogFragment fragment = new AllowListRemoveAllDialog();
        fragment.show(activity.getSupportFragmentManager(), AllowListRemoveAllDialog.class.toString());
    }

}
