package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog;

import jp.gr.java_conf.ofnhwx.callconfirm.provider.EditNumber;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.DB;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Param;
import jp.gr.java_conf.ofnhwx.olib.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Prefix/Suffixリストの追加・編集.
 * @author yuta
 */
public class EditNumberEditDialog extends DialogFragment implements OnClickListener {

    private TextView nameView;
    private TextView prefixView;
    private TextView suffixView;
    private CheckBox optionView;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_editnumber, null, false);
        nameView   = (TextView)view.findViewById(R.id.edit_number_name  );
        prefixView = (TextView)view.findViewById(R.id.edit_number_prefix);
        suffixView = (TextView)view.findViewById(R.id.edit_number_suffix);
        optionView = (CheckBox)view.findViewById(R.id.edit_number_option);
        view.findViewById(R.id.positive).setOnClickListener(this);
        view.findViewById(R.id.negative).setOnClickListener(this);
        //
        long id = getArguments().getLong(Param.ID);
        if (id != 0) {
            Uri uri = ContentUris.withAppendedId(EditNumber.List.CONTENT_URI, id);
            Cursor c = getActivity().getContentResolver().query(uri, null, null, null, null);
            try {
                c.moveToFirst();
                nameView  .setText   (c.getString(c.getColumnIndex(EditNumber.List.NAME  )));
                prefixView.setText   (c.getString(c.getColumnIndex(EditNumber.List.PREFIX)));
                suffixView.setText   (c.getString(c.getColumnIndex(EditNumber.List.SUFFIX)));
                optionView.setChecked(c.getInt   (c.getColumnIndex(EditNumber.List.OPTION)) != 0);
            } finally {
                c.close();
            }
        }
        //
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.add_editnumber_dialog_title);
        builder.setView(view);
        builder.setCancelable(true);
        return builder.create();
    }

    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.positive:
            long id = getArguments().getLong(Param.ID);
            String name   = nameView  .getText().toString();
            String prefix = prefixView.getText().toString();
            String suffix = suffixView.getText().toString();
            int    option = optionView.isChecked() ? 1 : 0;
            if (id == 0) {
                DB.EditNumberItem.insert(getActivity(), name, prefix, suffix, option);
            } else {
                DB.EditNumberItem.update(getActivity(), id, name, prefix, suffix, option);
            }
            break;
        }
        dismiss();
    }

    public static final void show(FragmentActivity activity, long id) {
        DialogFragment fragment = new EditNumberEditDialog();
        Bundle args = new Bundle();
        args.putLong(Param.ID, id);
        fragment.setArguments(args);
        fragment.show(activity.getSupportFragmentManager(), EditNumberEditDialog.class.toString());
    }

}
