package jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.dialog;

import jp.gr.java_conf.ofnhwx.callconfirm.utility.DB;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Param;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;

/**
 * Prefix/Suffixリストの削除.
 * @author yuta
 */
public class EditNumberRemoveDialog extends BaseRemoveConfirmDialog {

    public void onClick(DialogInterface dialog, int which) {
        long id = getArguments().getLong(Param.ID);
        DB.EditNumberItem.delete(getActivity(), id);
    }

    public static final void show(FragmentActivity activity, long id) {
        DialogFragment fragment = new EditNumberRemoveDialog();
        Bundle args = new Bundle();
        args.putLong(Param.ID, id);
        fragment.setArguments(args);
        fragment.show(activity.getSupportFragmentManager(), EditNumberRemoveDialog.class.toString());
    }

}
