package jp.gr.java_conf.ofnhwx.callconfirm.activity.list;

import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.BlockListFragment;
import android.os.Bundle;

/**
 * ブロックリスト - 登録された番号からの着信を拒否.
 * @author yuta
 */
public class BlockListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            replaceFragment(new BlockListFragment(), false);
        }
    }

}
