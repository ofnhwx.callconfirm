package jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm.ConfirmOption.ConfirmOptionCallback;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm.base.BaseConfirm;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.graphics.PixelFormat;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

/**
 * {@link LayoutParams#TYPE_SYSTEM_ALERT}な確認画面を表示する.
 * @author yuta
 */
public class ConfirmScreen extends BaseConfirm {

    private WindowManager wm;

    public ConfirmScreen(Activity activity, String number, String scheme) {
        this((Context)activity, number, scheme);
    }

    public ConfirmScreen(Service service, String number, String scheme) {
        this((Context)service, number, scheme);
    }

    private ConfirmScreen(Context context, String number, String scheme) {
        super(context, number, scheme);
        wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.callconfirm, null, false);
    }

    @Override
    public void onMappingViews() {
        frameView  = findViewById(R.id.callconfirm_frame   );
        mainView   = findViewById(R.id.callconfirm_main    );
        titleView  = findViewById(R.id.callconfirm_title   );
        lineView   = findViewById(R.id.callconfirm_line    );
        progress   = findViewById(R.id.callconfirm_progress);
        photoView  = findViewById(R.id.callconfirm_photo   );
        nameView   = findViewById(R.id.callconfirm_name    );
        numberView = findViewById(R.id.callconfirm_number  );
        positive   = findViewById(R.id.positive);
        negative   = findViewById(R.id.negative);
    }

    @Override
    public boolean showOptionMenu(ConfirmOptionCallback callback) {
        return false;
    }

    @Override
    public void doFinish() {
        wm.removeViewImmediate(rootView);
        if (context instanceof Service) {
            ((Service)context).stopSelf();
        } else if (context instanceof Activity) {
            ((Activity)context).finish();
        }
    }

    public void show() {
        Display display = wm.getDefaultDisplay();
        LayoutParams params = new LayoutParams();
        params.gravity   = Gravity.CENTER;
        params.format    = PixelFormat.TRANSPARENT;
        params.type      = LayoutParams.TYPE_SYSTEM_ALERT;
        params.flags     = LayoutParams.FLAG_DIM_BEHIND;
        params.dimAmount = 0.5f;
        params.width     = (int)(Math.min(display.getWidth(), display.getHeight()) * 0.9f);
        params.height    = LayoutParams.FILL_PARENT;
        wm.addView(rootView, params);
        displayViews();
    }

}
