package jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm;

import jp.gr.java_conf.ofnhwx.olib.ErrorReporter;
import android.content.Intent;
import android.os.Bundle;

/**
 * 不在着信の乗っ取りを担当.
 * @author yuta
 */
public class MissedCallActivity extends CallConfirm {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            /* ----- 定型処理 ----- */
            ErrorReporter.initialize(this);
            /* ----- ここまで ----- */
            String number = MissedCallQuery.getLastMissedCallNumber(this);
            Intent intent = new Intent();
            intent.putExtra(Intent.EXTRA_PHONE_NUMBER, number);
            setIntent(intent);
        }
        super.onCreate(savedInstanceState);
    }

}
