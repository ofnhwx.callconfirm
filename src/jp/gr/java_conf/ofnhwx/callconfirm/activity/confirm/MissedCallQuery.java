package jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm;

import jp.gr.java_conf.ofnhwx.callconfirm.utility.Utility;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.RemoteException;
import android.provider.CallLog;

import com.android.internal.telephony.ITelephony;

/**
 * 不在着信の通知を消すためにいろいろするクラス.
 * @author yuta
 */
public class MissedCallQuery extends AsyncQueryHandler {
    private Context mContext;

    private MissedCallQuery(Context context) {
        super(context.getContentResolver());
        mContext = context;
    }

    /**
     * 不在着信履歴の更新後に、再度未確認の不在着信を検索する.
     */
    @Override
    protected void onUpdateComplete(int token, Object cookie, int result) {
        MissedCallQuery query = new MissedCallQuery(mContext);
        String selection = getDefaultSelection().toString();
        String sortOrder = CallLog.Calls.DEFAULT_SORT_ORDER + " LIMIT 1";
        query.startQuery(0, null, CallLog.Calls.CONTENT_URI, null, selection, null, sortOrder);
    }

    /**
     * 未確認の不在着信がなくなったら、不在着信通知の消去を行う.
     */
    @Override
    protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
        if (cursor == null) {
            return;
        }
        try {
            if (cursor.getCount() == 0) {
                cancelMissedCallNotification(mContext);
            }
        } finally {
            cursor.close();
        }
    }

    /**
     * 不在着信の番号を取得、複数ある場合には選択画面を起動する.
     * @return 電話番号、取得できなかった場合(複数ある場合を含む)は'null'
     */
    public static final String getLastMissedCallNumber(Context context) {
        ContentResolver cr = context.getContentResolver();
        String selection = getDefaultSelection().toString();
        String sordOrder = CallLog.Calls.DEFAULT_SORT_ORDER;
        Cursor c = cr.query(CallLog.Calls.CONTENT_URI, null, selection, null, sordOrder);
        if (c == null) {
            return null;
        }
        try {
            int colnum = c.getColumnIndex(CallLog.Calls.NUMBER);
            String number;
            if (c.moveToFirst()) {
                number = c.getString(colnum);
            } else {
                cancelMissedCallNotification(context);
                return null;
            }
            while (c.moveToNext()) {
                if (!number.equals(c.getString(colnum))) {
                    MissedCallSelector.show(context);
                    return null;
                }
            }
            updateMissedCallLog(context, number);
            return number;
        } finally {
            c.close();
        }
    }

    /**
     * 指定された番号からの不在着信を確認済みにする.
     * @param context
     * @param number 確認済みにする電話番号
     */
    public static final void updateMissedCallLog(Context context, String number) {
        MissedCallQuery queryHandler = new MissedCallQuery(context);
        ContentValues values = new ContentValues();
        StringBuilder seletion = getDefaultSelection();
        values.put(CallLog.Calls.NEW, 0);
        seletion.append(" AND ");
        seletion.append(CallLog.Calls.NUMBER + "='" + number + "'");
        queryHandler.startUpdate(0, null, CallLog.Calls.CONTENT_URI, values, seletion.toString(), null);
    }

    /**
     * 不在着信を取得するための条件式.
     * @return "type=3(missed) AND new=1(acknowledged)"
     */
    public static final StringBuilder getDefaultSelection() {
        StringBuilder selection = new StringBuilder();
        selection.append(CallLog.Calls.TYPE + "=" + CallLog.Calls.MISSED_TYPE);
        selection.append(" AND ");
        selection.append(CallLog.Calls.NEW + "=1");
        return selection;
    }

    /**
     * 不在着信の通知を消す.
     * @param context
     */
    private static final void cancelMissedCallNotification(Context context) {
        // ITelephonyを取得、失敗したらそこで終了
        ITelephony phone = Utility.connectToTelephonyService(context);
        if (phone == null) {
            return;
        }
        // 非公開API
        try {
            phone.cancelMissedCallsNotification();
        } catch (RemoteException e) {
            e.printStackTrace();
            return;
        }
    }

}
