package jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm.ConfirmOption.ConfirmOptionCallback;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm.base.BaseConfirm;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Param;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;

/**
 * 確認の表示にダイアログを使う.
 * @author yuta
 */
public class ConfirmDialog extends DialogFragment implements OnKeyListener, OnClickListener {

    private class ConfirmDialogInternal extends BaseConfirm {

        public ConfirmDialogInternal(Context context, String number, String scheme) {
            super(context, number, scheme);
        }

        @Override
        public View onCreateView(LayoutInflater inflater) {
            return inflater.inflate(R.layout.callconfirm_dialog, null, false);
        }

        @Override
        public void onMappingViews() {
            frameView  = findViewById(R.id.callconfirm_frame   );
            mainView   = findViewById(R.id.callconfirm_main    );
            progress   = findViewById(R.id.callconfirm_progress);
            photoView  = findViewById(R.id.callconfirm_photo   );
            nameView   = findViewById(R.id.callconfirm_name    );
            numberView = findViewById(R.id.callconfirm_number  );
        }

        @Override
        public boolean showOptionMenu(ConfirmOptionCallback callback) {
            return false;
        }

        @Override
        public void doFinish() {
            getActivity().finish();
            dismiss();
        }

    }

    private ConfirmDialogInternal d;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //
        Bundle args = getArguments();
        String number = args.getString(Param.NUMBER);
        String scheme = args.getString(Param.SCHEME);
        d = new ConfirmDialogInternal(getActivity(), number, scheme);
        //
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.callconfirm_title);
        builder.setView(d.getView());
        builder.setOnKeyListener(this);
        builder.setPositiveButton(R.string.callconfirm_positive, this);
        builder.setNegativeButton(R.string.callconfirm_negative, this);
        return builder.create();
    }

    @Override
    public void onActivityCreated(Bundle args) {
        super.onActivityCreated(args);
        setHasOptionsMenu(true);
        setCancelable(true);
        d.displayViews();
    }

    public void onClick(DialogInterface dialog, int which) {
        int keyCode;
        switch (which) {
        case Dialog.BUTTON_POSITIVE:
            keyCode = KeyEvent.KEYCODE_CALL;
            break;
        case Dialog.BUTTON_NEGATIVE:
            keyCode = KeyEvent.KEYCODE_BACK;
            break;
        default:
            return;
        }
        KeyEvent event = new KeyEvent(KeyEvent.ACTION_UP, keyCode);
        d.onKey(getView(), keyCode, event);
    }

    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        return d.onKey(getView(), keyCode, event);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        KeyEvent event = new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK);
        d.onKey(getView(), KeyEvent.KEYCODE_BACK, event);
    }

    public static final void show(FragmentActivity activity, String number, String scheme) {
        Bundle args = new Bundle();
        args.putString(Param.NUMBER, number);
        args.putString(Param.SCHEME, scheme);
        DialogFragment dialog = new ConfirmDialog();
        dialog.setArguments(args);
        dialog.show(activity.getSupportFragmentManager(), ConfirmDialog.class.getName());
    }

}
