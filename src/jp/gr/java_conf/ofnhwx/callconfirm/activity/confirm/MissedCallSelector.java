package jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm;

import java.lang.ref.SoftReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.BaseActivity;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.info.ContactInfo;
import jp.gr.java_conf.ofnhwx.olib.utils.OBitmap;
import jp.gr.java_conf.ofnhwx.olib.utils.OUtil.NumberType;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * 不在着信が複数ある場合の選択機能を提供.
 * @author yuta
 */
public class MissedCallSelector extends Activity {

    // ダイアログ関連
    private static final int DIALOG_ID_MISSED_CALL_SELECTOR = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BaseActivity.finishOrder();
        showDialog(DIALOG_ID_MISSED_CALL_SELECTOR);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeDialog(DIALOG_ID_MISSED_CALL_SELECTOR);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DIALOG_ID_MISSED_CALL_SELECTOR:
            return new MissedCallSelectorDialog(this, id).create();
        }
        return null;
    }

    /**
     * 表示.
     * @param context
     */
    public static final void show(Context context) {
        Intent i = new Intent(context, MissedCallSelector.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(i);
    }

}

/**
 * 不在着信選択用ダイアログ.
 */
class MissedCallSelectorDialog extends AlertDialog.Builder implements OnItemClickListener, OnCancelListener {

    private Activity activity;
    private MissedCallSelectorAdapter adapter;

    public MissedCallSelectorDialog(Activity activity, int dialogId) {
        super(activity);
        this.activity = activity;
        // 不在着信選択リストの設定
        ListView listView = new ListView(activity);
        adapter = new MissedCallSelectorAdapter(activity);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        // ダイアログの設定
        setTitle(R.string.missed_calls);
        setView(listView);
        setOnCancelListener(this);
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String number = adapter.getItem(position).number;
        CallConfirm.show(activity, number, null);
        MissedCallQuery.updateMissedCallLog(activity, number);
        activity.finish();
    }

    public void onCancel(DialogInterface dialog) {
        activity.finish();
    }

}

/**
 * 不在着信の情報を格納するクラス.
 */
class MissedCallObject {

    /** 電話番号 */
    public final String number;
    /**  表示名  */
    public final String name;
    /** 最終着信 */
    public final long time;
    /** 着信回数 */
    public int count;

    public MissedCallObject(String number, String name, long time) {
        this.number = number;
        this.name   = name;
        this.time   = time;
        this.count  = 1;
    }

}

/**
 * アイテムの表示に使用するアダプタ.
 */
class MissedCallSelectorAdapter extends ArrayAdapter<MissedCallObject> {

    private static final SimpleDateFormat sdf = new SimpleDateFormat();

    private String UNKNOWN;
    private String PRIVATE;
    private String PAYPHONE;

    private Context context;
    private LayoutInflater inflater;

    private static class TagObject {
        public ImageView photo;
        public TextView  name;
        public TextView  number;
        public TextView  time;
        public TextView  count;
        public GetCachedPhotoTask task;
    }

    public MissedCallSelectorAdapter(Context context) {
        super(context, R.layout.item_missedcalls);
        this.context = context;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //
        UNKNOWN  = context.getString(R.string.unknown_number);
        PRIVATE  = context.getString(R.string.private_number);
        PAYPHONE = context.getString(R.string.payphone_number);
        //
        new SearchMissedCallTask(this).execute();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        TagObject tag;
        if (view == null) {
            view = inflater.inflate(R.layout.item_missedcalls, null);
            //
            tag = new TagObject();
            tag.photo  = (ImageView)view.findViewById(R.id.list_photo );
            tag.name   = (TextView )view.findViewById(R.id.list_name  );
            tag.number = (TextView )view.findViewById(R.id.list_number);
            tag.time   = (TextView )view.findViewById(R.id.list_time  );
            tag.count  = (TextView )view.findViewById(R.id.list_count );
            view.setTag(tag);
        } else {
            tag = (TagObject)view.getTag();
        }
        // 写真を表示
        // + 写真を初期化する
        // + 古いタスクをキャンセルする
        // + 新しいタスクを作成し実行する
        MissedCallObject missedCall = getItem(position);
        GetCachedPhotoTask oldTask = tag.task;
        GetCachedPhotoTask newTask = new GetCachedPhotoTask(context, tag.photo, missedCall.number);
        if (oldTask != null && !oldTask.isCancelled()) {
            oldTask.cancel(true);
        }
        tag.task = (GetCachedPhotoTask)newTask.execute();
        // 名前・電話番号を表示
        String number;
        switch (NumberType.get(missedCall.number)) {
        case UNKNOWN:
            number = UNKNOWN;
            break;
        case PRIVATE:
            number = PRIVATE;
            break;
        case PAYPHONE:
            number = PAYPHONE;
            break;
        default:
            number = PhoneNumberUtils.formatNumber(missedCall.number);
        }
        if (TextUtils.isEmpty(missedCall.name)) {
            tag.name  .setText(number);
            tag.number.setText(null);
        } else {
            tag.name  .setText(missedCall.name);
            tag.number.setText(number);
        }
        // 時間を表示
        tag.time.setText(sdf.format(new Date(missedCall.time)));
        // 回数を表示
        if (missedCall.count > 1) {
            tag.count.setText(String.format("(%d)", missedCall.count));
        } else {
            tag.count.setText(null);
        }
        // Viewを返す
        return view;
    }

}

/**
 * 非同期で不在着信を検索しアダプタに登録する.
 */
class SearchMissedCallTask extends AsyncTask<Void, MissedCallObject, Void> {

    private Context context;
    private ArrayAdapter<MissedCallObject> adapter;
    private HashMap<String, MissedCallObject> missedCalls = new HashMap<String, MissedCallObject>();

    public SearchMissedCallTask(ArrayAdapter<MissedCallObject> adapter) {
        this.context = adapter.getContext();
        this.adapter = adapter;
    }

    @Override
    protected void onProgressUpdate(MissedCallObject... values) {
        MissedCallObject missedCall = values[0];
        if (missedCalls.containsKey(missedCall.number)) {
            missedCalls.get(missedCall.number).count++;
        } else {
            missedCalls.put(missedCall.number, missedCall);
            adapter.add(missedCall);
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        // 通話履歴から不在着信(番号、名前、日付)を検索
        ContentResolver cr = context.getContentResolver();
        String[] projection = {
            CallLog.Calls.NUMBER,
            CallLog.Calls.CACHED_NAME,
            CallLog.Calls.DATE
        };
        String selection = MissedCallQuery.getDefaultSelection().toString();
        String sortOrder = CallLog.Calls.DEFAULT_SORT_ORDER;
        Cursor c = cr.query(CallLog.Calls.CONTENT_URI, projection, selection, null, sortOrder);
        if (c == null) {
            return null;
        }
        // Cursor -> HashMap<String, MissedCallObject>
        try {
            int colNumner = c.getColumnIndex(CallLog.Calls.NUMBER);
            int colName   = c.getColumnIndex(CallLog.Calls.CACHED_NAME);
            int colTime   = c.getColumnIndex(CallLog.Calls.DATE);
            while (c.moveToNext()) {
                String number = c.getString(colNumner);
                if (missedCalls.containsKey(number)) {
                    publishProgress(missedCalls.get(number));
                } else {
                    String name = c.getString(colName);
                    long   time = c.getLong  (colTime);
                    publishProgress(new MissedCallObject(number, name, time));
                }
            }
        } finally {
            c.close();
        }
        return null;
    }

}

/**
 * コンタクトの写真を非同期で取得.
 */
class GetCachedPhotoTask extends AsyncTask<Void, Void, Bitmap> {

    private static final Map<String, SoftReference<Bitmap>> photos = new ConcurrentHashMap<String, SoftReference<Bitmap>>();

    private static int size = 0;
    @SuppressWarnings("unused")
    private static Bitmap unknown;
    private static Bitmap noimage;

    private Context   context;
    private ImageView photo;
    private String    number;

    public GetCachedPhotoTask(Context context,ImageView photo, String number) {
        this.context = context;
        this.photo   = photo;
        this.number  = number;
        if (size == 0.0f) {
            size = (int)(40.0f * context.getResources().getDisplayMetrics().density);
            unknown = OBitmap.decodeResource(context, R.drawable.contact_unknown, size);
            noimage = OBitmap.decodeResource(context, R.drawable.contact_noimage, size);
        }
    }

    @Override
    protected void onPreExecute() {
        photo.setImageBitmap(noimage);
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            return null;
        }
        if (isCancelled()) {
            return null;
        }
        // 写真をキャッシュから取得
        SoftReference<Bitmap> cache = photos.get(number);
        if (cache != null) {
            Bitmap bitmap = cache.get();
            if (bitmap != null) {
                return bitmap;
            }
        }
        // 写真を電話帳から取得
        ContactInfo info = new ContactInfo(context, number);
        Bitmap bitmap = OBitmap.resizeBitmap(info.getPhoto(0), size, true);
        photos.put(number, new SoftReference<Bitmap>(bitmap));
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if (result == null) {
            return;
        }
        photo.setImageBitmap(result);
    }

}
