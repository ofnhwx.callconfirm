package jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.fragment.adapter.EditNumberAdapter;
import jp.gr.java_conf.ofnhwx.callconfirm.provider.EditNumber;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.DB;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.info.AllowListInfo;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.info.ContactInfo;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;

/**
 * 許可リストの制御、Prefix/Suffixの設定を行う.
 * @author yuta
 */
public class ConfirmOption implements OnCheckedChangeListener, OnItemClickListener, OnKeyListener {

    /**
     * Prefix/Suffixのアイテムが選択された際のCallbackイベント.
     */
    public interface ConfirmOptionCallback {
        /**
         * <code>prefix, suffix, option</code>を返すので適当に使ってね.
         * @param prefix 接頭辞
         * @param suffix 接尾辞
         * @param option true:頭の<code>0</code>は取るべき
         * @param cancel
         */
        public void onOptionCallback(String prefix, String suffix, boolean option, boolean cancel);
    }

    private ConfirmOptionCallback parent;
    private Context context;
    private String  number ;

    private LinearLayout rootView;
    private CheckBox allowView;
    private ListView listView;
    private EditNumberAdapter adapter;

    private ConfirmOption(ConfirmOptionCallback parent, Context context, String number) {
        this.parent  = parent ;
        this.context = context;
        this.number  = number ;
        //
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rootView = (LinearLayout)inflater.inflate(R.layout.callconfirm_option, null, false);
        allowView = (CheckBox)rootView.findViewById(R.id.callconfirm_allow);
        listView  = (ListView)rootView.findViewById(android.R.id.list);
        //
        Cursor c = context.getContentResolver().query(EditNumber.List.CONTENT_URI, null, null, null, null);
        adapter = new EditNumberAdapter(context, number);
        listView.addHeaderView(inflater.inflate(R.layout.item_editnumber, null, false));
        listView.setAdapter(adapter);
        adapter.swapCursor(c);
        //
        customizeViews();
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        AllowListInfo info = new AllowListInfo(context, number);
        if (isChecked) {
            if (!info.isAllowNumber()) {
                ContactInfo contact = new ContactInfo(context, number);
                DB.AllowListItem.insert(context, contact.isFound() ? contact.getName(0) : null, number);
            }
        } else {
            if (info.isAllowNumber()) {
                DB.AllowListItem.delete(context, info.getId());
            }
        }
    }

    public void onItemClick(AdapterView<?> l, View v, int pos, long id) {
        String  prefix;
        String  suffix;
        boolean option;
        if (pos < listView.getHeaderViewsCount()) {
            prefix = null;
            suffix = null;
            option = false;
        } else {
            Cursor c = adapter.getCursor();
            prefix = c.getString(c.getColumnIndex(EditNumber.List.PREFIX));
            suffix = c.getString(c.getColumnIndex(EditNumber.List.SUFFIX));
            option = c.getLong(c.getColumnIndex(EditNumber.List.OPTION)) == 1;
        }
        parent.onOptionCallback(prefix, suffix, option, false);
        close();
    }

    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_UP) {
            return false;
        }
        switch (keyCode) {
        case KeyEvent.KEYCODE_BACK:
        case KeyEvent.KEYCODE_MENU:
            parent.onOptionCallback(null, null, false, true);
            close();
            return true;
        }
        return false;
    }

    public static final void show(Context context, ViewGroup parent, String number, ConfirmOptionCallback callback) {
        ConfirmOption option = new ConfirmOption(callback, context, number);
        parent.addView(option.rootView);
    }

    private void close() {
        adapter.swapCursor(null);
        ((ViewGroup)rootView.getParent()).removeView(rootView);
    }

    private void customizeViews() {
        rootView.setBackgroundResource(android.R.drawable.dialog_frame);
        // 許可リストの設定
        allowView.setOnCheckedChangeListener(this);
        allowView.setOnKeyListener(this);
        allowView.setFocusable(true);
        allowView.setFocusableInTouchMode(true);
        allowView.requestFocus();
        allowView.setChecked(new AllowListInfo(context, number).isAllowNumber());
        // リストビューの設定
        listView.setOnItemClickListener(this);
        listView.setOnKeyListener(this);
        listView.setFocusable(true);
        listView.setFocusableInTouchMode(true);
        listView.setCacheColorHint(Color.TRANSPARENT);
    }

}
