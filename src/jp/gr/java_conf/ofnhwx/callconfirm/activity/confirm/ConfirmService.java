package jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.MainPreference;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Param;
import jp.gr.java_conf.ofnhwx.olib.base.BaseService;
import jp.gr.java_conf.ofnhwx.olib.utils.OPreference;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.IBinder;
import android.telephony.PhoneNumberUtils;

/**
 * {@link ConfirmScreen}を表示するサービス.
 * @author yuta
 */
public class ConfirmService extends BaseService {

    private Notification notification;
    private PendingIntent contentIntent;
    private String number;
    private String scheme;

    private ConfirmScreen screen;

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForeground();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
        scheme = intent.getStringExtra(Param.SCHEME);
        updateNotify();
        //
        if (screen == null) {
            screen = new ConfirmScreen(this, number, scheme);
            screen.show();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void startForeground() {
        updateNotify();
        startForegroundCompat(0, notification);
    }

    private void stopForeground() {
        stopForegroundCompat();
        cancelNotify();
    }

    private void updateNotify() {
        OPreference prefs = OPreference.getInstance(this);
        if (number == null) {
            number = prefs.getString(R.string.key_last_call_number, null);
            scheme = prefs.getString(R.string.key_last_call_scheme, null);
        } else {
            prefs.write(R.string.key_last_call_number, number);
            prefs.write(R.string.key_last_call_scheme, scheme);
        }
        //
        if (notification == null) {
            notification = new Notification();
            notification.icon  = R.drawable.callconfirm;
            notification.flags = Notification.FLAG_ONGOING_EVENT;
        }
        //
        CharSequence title = getText(R.string.app_name);
        CharSequence text  = number == null ? null : PhoneNumberUtils.formatNumber(number);
        if (number == null) {
            Intent intent = new Intent(this, MainPreference.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            contentIntent = PendingIntent.getActivity(this, 0, intent, 0);
        } else {
            Intent intent = new Intent(this, ConfirmService.class);
            intent.putExtra(Intent.EXTRA_PHONE_NUMBER, number);
            intent.putExtra(Param.SCHEME, scheme);
            contentIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
        notification.setLatestEventInfo(this, title, text, contentIntent);
        //
        NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        nm.notify(0, notification);
    }

    private void cancelNotify() {
        NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        nm.cancel(0);
    }

}
