package jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm;

import jp.gr.java_conf.ofnhwx.olib.ErrorReporter;
import jp.gr.java_conf.ofnhwx.olib.utils.OUtil;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;

/**
 * ダイアル画面の置き換えを担当.
 * @author yuta
 */
public class DialerActivity extends CallConfirm {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            /* ----- 定型処理 ----- */
            ErrorReporter.initialize(this);
            /* ----- ここまで ----- */
            Intent intent = getIntent();
            String number = PhoneNumberUtils.stripSeparators(intent.getDataString());
            if (TextUtils.isEmpty(number) || PhoneNumberUtils.isEmergencyNumber(number)) {
                intent.putExtra(Intent.EXTRA_PHONE_NUMBER, (String)null);
                showOtherDialer(number == null ? "" : number);
            } else {
                intent.putExtra(Intent.EXTRA_PHONE_NUMBER, number);
            }
        }
        super.onCreate(savedInstanceState);
    }

    /**
     * 他のダイアル画面を表示する.<br>
     * <pre>
     * ACTION_DIALに対応しているアプリが複数ある場合は問題あり
     * ただ、どうしようもないし致命的でもないのでとりあえずは放置
     * </pre>
     * @param number
     */
    private void showOtherDialer(String number) {
        Class<?> clazz = DialerActivity.class;
        try {
            OUtil.setComponentEnabled(this, clazz, false);
            startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + number)));
        } finally {
            OUtil.setComponentEnabled(this, clazz, true);
        }
    }

}
