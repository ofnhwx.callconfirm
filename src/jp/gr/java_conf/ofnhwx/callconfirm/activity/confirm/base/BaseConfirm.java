package jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm.base;

import java.util.EnumSet;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm.ConfirmOption;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm.ConfirmOption.ConfirmOptionCallback;
import jp.gr.java_conf.ofnhwx.callconfirm.receiver.OutgoingCallReceiver;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Param;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.info.ContactInfo;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.thread.GetContactTask;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.thread.GetContactTask.GetContactCallback;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.thread.GetPhotoTask;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.thread.GetPhotoTask.GetPhotoCallback;
import jp.gr.java_conf.ofnhwx.olib.utils.OBitmap;
import jp.gr.java_conf.ofnhwx.olib.utils.OPhone;
import jp.gr.java_conf.ofnhwx.olib.utils.OUtil.NumberType;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.telephony.PhoneNumberUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * 確認画面のもと.
 * @author yuta
 */
public abstract class BaseConfirm
        implements OnClickListener, OnKeyListener,
                   ConfirmOptionCallback, GetContactCallback, GetPhotoCallback  {

    // 状態の管理
    private static enum State {
        HAS_TITLE,
        HAS_BUTTONS,
        FINISHING,
        OPTION_MODE
    }
    private EnumSet<State> state = EnumSet.noneOf(State.class);

    //
    protected Context context;

    // 各種ビュー
    protected LinearLayout rootView  ;
    protected FrameLayout  frameView ;
    protected LinearLayout mainView  ;
    protected TextView     titleView ;
    protected ImageView    lineView  ;
    protected ProgressBar  progress  ;
    protected ImageView    photoView ;
    protected TextView     nameView  ;
    protected TextView     numberView;
    protected Button       positive  ;
    protected Button       negative  ;

    // 画面表示に使用するパラメータ
    private String  number;
    private String  scheme;
    private String  prefix;
    private String  suffix;
    private boolean option;
    private int photoSize;

    // コンタクトの情報
    private ContactInfo contactInfo;
    private int index;

    // タスク
    private GetContactTask getContactTask;
    private GetPhotoTask   getPhotoTask  ;

    /**
     * ビューの作成を行う.
     * @param inflater
     */
    public abstract View onCreateView(LayoutInflater inflater);

    /**
     * ビューの割当てを行う.
     * <ul>
     *  <li>{@link #frameView}</li>
     *  <li>{@link #mainView}</li>
     *  <li>{@link #titleView}:省略可</li>
     *  <li>{@link #lineView}:省略可</li>
     *  <li>{@link #progress}</li>
     *  <li>{@link #photoView}</li>
     *  <li>{@link #nameView}</li>
     *  <li>{@link #numberView}</li>
     *  <li>{@link #positive}:省略可</li>
     *  <li>{@link #negative}:省略可</li>
     * </ul>
     * @see #findViewById(int)
     */
    public abstract void onMappingViews();

    /**
     * オプションメニューを表示する.
     * @param callback
     * @return
     */
    public abstract boolean showOptionMenu(ConfirmOptionCallback callback);

    /**
     * 終了処理を行う.
     */
    public abstract void doFinish();

    /**
     * {@link #onCreateViews()}を呼出すので、継承したクラスは
     * {@link #findViewById(int)}を使用して必要な{@link View}
     * の設定を行うこと.
     * @param context コンテキスト
     * @param number 電話番号
     * @param scheme スキーマ
     */
    public BaseConfirm(Context context, String number, String scheme) {
        this.context = context;
        this.number  = number;
        this.scheme  = scheme;
        this.photoSize = Param.getMyPictureSize(context);
        // ビューの作成
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rootView = (LinearLayout)onCreateView(inflater);
        // ビューの割当
        onMappingViews();
        if (frameView == null || mainView == null ||
                progress  == null || photoView  == null ||
                nameView  == null || numberView == null) {
            throw new IllegalStateException();
        }
        if (titleView != null && lineView != null) {
            state.add(State.HAS_TITLE);
        }
        if (positive != null && negative != null) {
            state.add(State.HAS_BUTTONS);
        }
        // ビューの設定
        customizeViews();
    }

    /**
     * ビューの取得.
     * @return
     */
    public View getView() {
        return rootView;
    }

    /**
     * ビューの表示開始.
     */
    public void displayViews() {
        if (NumberType.get(number) == NumberType.NORMAL) {
            getContactTask = new GetContactTask(context, number, this);
            getContactTask.execute();
        } else {
            if (state.contains(State.HAS_BUTTONS)) {
                positive.setEnabled(false);
            }
            displayPhoto();
            displayName();
        }
        displayNumber();
    }

    /**
     * ボタン押下時のイベント.
     */
    public void onClick(View v) {
        if (state.contains(State.OPTION_MODE)) {
            return;
        }
        action(v.getId() == R.id.positive);
    }

    /**
     * キー入力時のイベント.
     */
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (state.contains(State.OPTION_MODE)) {
            return false;
        }
        if (event.getAction() != KeyEvent.ACTION_UP) {
            return true;
        }
        switch (keyCode) {
        case KeyEvent.KEYCODE_VOLUME_UP:
            if (index < contactInfo.getCount() - 1) {
                index = index + 1;
                displayName();
                displayPhoto();
            }
            return true;
        case KeyEvent.KEYCODE_VOLUME_DOWN:
            if (index > 0) {
                index = index - 1;
                displayName();
                displayPhoto();
            }
            return true;
        case KeyEvent.KEYCODE_CALL:
            action(true);
            return true;
        case KeyEvent.KEYCODE_BACK:
            action(false);
            return true;
        case KeyEvent.KEYCODE_MENU:
            if (NumberType.get(number) == NumberType.NORMAL) {
                setOptionMode(true);
                if (!showOptionMenu(this)) {
                    ConfirmOption.show(context, frameView, number, this);
                }
            }
            return true;
        }
        return false;
    }

    /**
     * オプションメニューでの設定を受取って反映する.
     */
    public void onOptionCallback(String prefix, String suffix, boolean option, boolean cancel) {
        setOptionMode(false);
        if (cancel) {
            return;
        }
        this.prefix = prefix;
        this.suffix = suffix;
        this.option = option;
        displayNumber();
    }

    /**
     * コンタクトの情報を受取って表示する.
     */
    public void onGetContactCallback(ContactInfo info) {
        contactInfo = info;
        displayName();
        displayPhoto();
    }

    /**
     * コンタクトの写真を受取って表示する.
     */
    public void onGetPhotoCallback(Bitmap photo) {
        if (photo == null) {
            photoView.setImageBitmap(OBitmap.decodeResource(context, R.drawable.contact_unknown, photoSize, true));
        } else {
            photoView.setImageBitmap(OBitmap.resizeBitmap(photo, photoSize, true));
        }
    }

    /**
     * {@link #onCreateView(LayoutInflater)}で生成した{@link View}から、<code>id</code>で検索する.
     * @param id 検索する{@link View}の<code>id</code>
     * @return {@link View}を継承したクラスのインスタンス
     */
    @SuppressWarnings("unchecked")
    protected <T extends View> T findViewById(int id) {
        return (T)rootView.findViewById(id);
    }

    /**
     * 画面に名前を表示する.
     */
    private void displayName() {
        if (contactInfo == null) {
            nameView.setText(R.string.callconfirm_unknown);
            return;
        }
        if (contactInfo.getCount() > 1) {
            String text = String.format("(%d/%d)", index + 1, contactInfo.getCount());
            nameView.setText(contactInfo.getName(index) + text);
        } else {
            nameView.setText(contactInfo.getName(index));
        }
    }

    /**
     * 画面に番号を表示する.
     */
    private void displayNumber() {
        switch (NumberType.get(number)) {
        case UNKNOWN:
            numberView.setText(R.string.unknown_number);
            break;
        case PRIVATE:
            numberView.setText(R.string.private_number);
            break;
        case PAYPHONE:
            numberView.setText(R.string.payphone_number);
            break;
        default:
            String _prefix = prefix == null ? "" : prefix + " ";
            String _suffix = suffix == null ? "" : " " + suffix;
            String _number;
            if (option && number.startsWith("0")) {
                _number = PhoneNumberUtils.formatNumber(number.substring(1));
            } else {
                _number = PhoneNumberUtils.formatNumber(number);
            }
            numberView.setText(_prefix + _number + _suffix);
            break;
        }
    }

    /**
     * 画面に写真を表示する.
     */
    private void displayPhoto() {
        if (contactInfo == null) {
            onGetPhotoCallback(null);
        } else {
            cancelTask(getPhotoTask);
            getPhotoTask = new GetPhotoTask(contactInfo, index, this);
            getPhotoTask.execute();
        }
    }

    /**
     * すべての非同期タスクをキャンセルする.
     */
    private void cancelTasks() {
        cancelTask(getContactTask);
        cancelTask(getPhotoTask);
    }

    /**
     * 非同期タスクをキャンセルする.
     * @param task キャンセルするタスク
     */
    private <T extends AsyncTask<?, ?, ?>> void cancelTask(T task) {
        if (task != null && !task.isCancelled()) {
            task.cancel(true);
        }
    }

    /**
     * ボタン押下時のイベント.
     * @param positive <code>true</code>の場合、電話の発信を行う
     */
    private void action(boolean positive) {
        synchronized (state) {
            if (state.contains(State.FINISHING)) {
                return;
            } else {
                state.add(State.FINISHING);
            }
        }
        if (positive) {
            StringBuilder sb = new StringBuilder();
            if (prefix != null) {
                sb.append(prefix);
            }
            if (option && number.startsWith("0")) {
                sb.append(number.substring(1));
            } else {
                sb.append(number);
            }
            if (suffix != null) {
                sb.append(suffix);
            }
            OutgoingCallReceiver.setStateConfirmed(context, sb.toString());
            OPhone.call(context, scheme, sb.toString());
        }
        cancelTasks();
        doFinish();
    }

    /**
     * オプションモードの切替え.
     * @param mode
     */
    private void setOptionMode(boolean mode) {
        synchronized (state) {
            if (mode) {
                state.add(State.OPTION_MODE);
            } else {
                state.remove(State.OPTION_MODE);
            }
        }
        if (state.contains(State.HAS_BUTTONS)) {
            positive.setFocusable(!mode);
            negative.setFocusable(!mode);
            negative.setFocusableInTouchMode(!mode);
        }
    }

    /**
     * 各ビューの設定.
     */
    private void customizeViews() {
        // 写真の既定値
        progress.setLayoutParams(new FrameLayout.LayoutParams(photoSize, photoSize));
        photoView.setImageBitmap(OBitmap.decodeResource(context, R.drawable.contact_noimage, photoSize));
        // 名前, 番号の設定
        int color = Param.getMyTextColor(context);
        int size  = Param.getMyTextSize (context);
        nameView.setTextSize (size );
        nameView.setTextColor(color);
        numberView.setTextSize (size );
        numberView.setTextColor(color);
        // タイトルあり
        if (state.contains(State.HAS_TITLE)) {
            // タイトルの設定
            titleView.setText(R.string.callconfirm_title);
            titleView.setTextSize (Param.getMyTitleSize (context));
            titleView.setTextColor(Param.getMyTitleColor(context));
            // テーマの設定
            switch (Param.getMyTheme(context)) {
            case Param.THEMA_WHITE:
                mainView.setBackgroundResource(R.drawable.background_white);
                break;
            case Param.THEMA_NONE:
                mainView.setBackgroundResource(0);
                break;
            case Param.THEMA_DIALOG:
                mainView.setBackgroundResource(android.R.drawable.dialog_frame);
                break;
            default: // = Param.THEME_BLACK
                mainView.setBackgroundResource(R.drawable.background_black);
                break;
            }
            lineView.setImageResource(android.R.drawable.divider_horizontal_dark);
        }
        // ボタンあり
        if (state.contains(State.HAS_BUTTONS)) {
            // イベントの設定
            positive.setOnClickListener(this);
            negative.setOnClickListener(this);
            positive.setOnKeyListener(this);
            negative.setOnKeyListener(this);
            // フォーカスの設定
            positive.setFocusable(true);
            negative.setFocusable(true);
            negative.setFocusableInTouchMode(true);
            negative.requestFocus();
        }
    }

}
