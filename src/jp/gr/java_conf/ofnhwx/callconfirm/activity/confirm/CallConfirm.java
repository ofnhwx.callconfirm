package jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm.ConfirmOption.ConfirmOptionCallback;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm.base.BaseConfirm;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.BaseActivity;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Param;
import jp.gr.java_conf.ofnhwx.olib.ErrorReporter;
import jp.gr.java_conf.ofnhwx.olib.utils.OPreference;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.Toast;

/**
 * メインと思われる確認画面.
 * @author yuta
 */
public class CallConfirm extends FragmentActivity {

    private class CallConfirmInternal extends BaseConfirm {

        public CallConfirmInternal(Context context, String number, String scheme) {
            super(context, number, scheme);
        }

        @Override
        public View onCreateView(LayoutInflater inflater) {
            return inflater.inflate(R.layout.callconfirm, null, false);
        }

        @Override
        public void onMappingViews() {
            frameView  = findViewById(R.id.callconfirm_frame   );
            mainView   = findViewById(R.id.callconfirm_main    );
            titleView  = findViewById(R.id.callconfirm_title   );
            lineView   = findViewById(R.id.callconfirm_line    );
            progress   = findViewById(R.id.callconfirm_progress);
            photoView  = findViewById(R.id.callconfirm_photo   );
            nameView   = findViewById(R.id.callconfirm_name    );
            numberView = findViewById(R.id.callconfirm_number  );
            positive   = findViewById(R.id.positive);
            negative   = findViewById(R.id.negative);
        }

        @Override
        public boolean showOptionMenu(ConfirmOptionCallback callback) {
            return false;
        }

        @Override
        public void doFinish() {
            finish();
        }

    }

    private CallConfirmInternal c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ErrorReporter.initialize(this);
        BaseActivity.finishOrder();
        // パラメータの受取り
        String number = getIntent().getStringExtra(Intent.EXTRA_PHONE_NUMBER);
        String scheme = getIntent().getStringExtra(Param.SCHEME);
        if (TextUtils.isEmpty(number)) {
            finish();
            return;
        }
        //
        OPreference prefs = OPreference.getInstance(this);
        if (prefs.getBoolean(R.string.key_use_service, false)) {
            startService(this, number, scheme);
            finish();
            return;
        } else if (prefs.getBoolean(R.string.key_use_dialog, false)) {
            ConfirmDialog.show(this, number, scheme);
        } else {
            c = new CallConfirmInternal(this, number, scheme);
            c.displayViews();
            //
            FrameLayout layout = new FrameLayout(this);
            setContentView(layout, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
            //
            Display display = getWindowManager().getDefaultDisplay();
            int width  = (int)(Math.min(display.getWidth(), display.getHeight()) * 0.9f);
            int height = LayoutParams.FILL_PARENT;
            LayoutParams params = new LayoutParams(width, height);
            params.gravity = Gravity.CENTER;
            layout.addView(c.getView(), params);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isFinishing() && OPreference.getInstance(this).getBoolean(R.string.key_use_toast, false)) {
            Toast.makeText(this, "Searching for contacts...", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        return c.onKey(c.getView(), event.getKeyCode(), event);
    }

    public static final void show(Context context, String number, String uri) {
        // Schemeの取得
        String scheme;
        if (uri == null) {
            scheme = null;
        } else {
            scheme = Uri.parse(uri).getScheme();
        }
        // Activityの起動
        Intent activity = new Intent(context, CallConfirm.class);
        activity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.putExtra(Intent.EXTRA_PHONE_NUMBER, PhoneNumberUtils.stripSeparators(number));
        activity.putExtra(Param.SCHEME, scheme);
        context.startActivity(activity);
    }

    private static final void startService(Context context, String number, String scheme) {
        Intent service = new Intent(context, ConfirmService.class);
        service.putExtra(Intent.EXTRA_PHONE_NUMBER, PhoneNumberUtils.stripSeparators(number));
        service.putExtra(Param.SCHEME, scheme);
        context.startService(service);
    }

}
