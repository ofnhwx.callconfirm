package jp.gr.java_conf.ofnhwx.callconfirm.receiver;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.list.BlockHistoryActivity;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Param;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.Utility;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.info.BlockListInfo;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.info.ContactInfo;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.thread.AddBlockHistory;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.thread.AutoEndCall;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.thread.DeleteCallLog;
import jp.gr.java_conf.ofnhwx.olib.ErrorReporter;
import jp.gr.java_conf.ofnhwx.olib.utils.OPreference;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;

/**
 * 電話の状態を取得するクラス(主に着信の制御に使用).
 * @author yuta
 */
public class PhoneStateReceiver extends BroadcastReceiver {

    private static enum State {
        RINGING,
        OFFHOOK,
        IDLE,
        UNKNOWN;
        public static final State get(String state) {
            if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                return State.RINGING;
            }
            if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                return State.OFFHOOK;
            }
            if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                return State.IDLE;
            }
            return State.UNKNOWN;
        }
    }
    private static State lastState = State.IDLE;

    @Override
    public void onReceive(Context context, Intent intent) {
        ErrorReporter.initialize(context);
        OPreference prefs = OPreference.getInstance(context);
        String state  = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        String number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
        switch (State.get(state)) {
        case RINGING:
            checkEndCall(context, number);
            break;
        case OFFHOOK:
            boolean enableAutoEnd = prefs.getBoolean(R.string.key_auto_reject, false);
            boolean endIncoming   = prefs.getBoolean(R.string.key_auto_reject_incoming, false);
            boolean isEmergency   = number == null ? false : PhoneNumberUtils.isEmergencyNumber(number);
            if (enableAutoEnd) {
                if (isEmergency) {
                    // 緊急通報の番号は終了させない
                } else if (endIncoming || lastState == State.IDLE) {
                    int wait = Param.getAutoEndTime(context);
                    AutoEndCall.execute(context, wait);
                }
            }
            break;
        case IDLE:
            if (prefs.getBoolean(R.string.key_vibrate_idle, false)) {
                Utility.vibrate(context);
            }
            break;
        default:
            break;
        }
        lastState = State.get(state);
    }

    /**
     * 着信番号を確認し、必要に応じて{@link #endCallInternal(Context, String)}を呼出す.
     * @param context
     * @param number
     */
    private void checkEndCall(Context context, String number) {
        OPreference prefs = OPreference.getInstance(context);
        // すべての着信を拒否する
        if (prefs.getBoolean(R.string.key_callblock_all, false)) {
            endCallInternal(context, number);
        } else if (number == null) {
            // 非通知着信を拒否する
            if (prefs.getBoolean(R.string.key_enable_callblock, false)) {
                endCallInternal(context, null);
            }
        } else {
            // ブロックリストに登録されている
            if (prefs.getBoolean(R.string.key_use_blocklist, false)) {
                if (new BlockListInfo(context, number).isBlockNumber()) {
                    endCallInternal(context, number);
                }
            }
            // 電話帳登録外 + 電話帳のお気に入り以外の拒否
            boolean blockNoContacts = prefs.getBoolean(R.string.key_callblock_nocontacts, false);
            boolean blockNoFavorite = prefs.getBoolean(R.string.key_callblock_nofavorite, false);
            if (blockNoContacts || blockNoFavorite) {
                ContactInfo contactInfo = new ContactInfo(context, number);
                if ((blockNoContacts && !contactInfo.isFound()) ||
                    (blockNoFavorite && contactInfo.isFound() && !contactInfo.isFavorite())) {
                    endCallInternal(context, number);
                }
            }
        }
    }

    /**
     * 通話の終了.
     * @param context
     * @param number
     */
    private void endCallInternal(Context context, String number) {
        OPreference prefs = OPreference.getInstance(context);
        // ITelephonyを取得、失敗したらそこで終了
        ITelephony phone = Utility.connectToTelephonyService(context);
        if (phone == null) {
            return;
        }
        // 通話の終了 + Toastの表示
        String reject = (number == null ? context.getString(R.string.unknown_number) : number);
        try {
            boolean answerBeforeEnd = prefs.getBoolean(R.string.key_answer_before_end, false);
            phone.silenceRinger();
            if (answerBeforeEnd) {
                phone.answerRingingCall();
            }
            phone.endCall();
            String message = context.getString(R.string.callblock_block) + reject;
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        } catch (RemoteException e) {
            e.printStackTrace();
            return;
        }
        // ブロックした履歴の登録 + 通話履歴の削除
        boolean deleteHistory = prefs.getBoolean(R.string.key_delete_history, false);
        AddBlockHistory.execute(context, number);
        if (deleteHistory) {
            DeleteCallLog.execute(context, number);
        }
        // ブロックした着信の通知
        boolean blockNotify = prefs.getBoolean(R.string.key_block_notify, false);
        if (blockNotify) {
            NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notification = new Notification();
            Intent intent = new Intent(context, BlockHistoryActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, 0);
            String title = context.getString(R.string.callblock_notify);
            notification.icon  = R.drawable.blocknotify;
            notification.flags = Notification.FLAG_AUTO_CANCEL;
            notification.setLatestEventInfo(context, title, reject, contentIntent);
            nm.notify(0, notification);
        }
    }

}
