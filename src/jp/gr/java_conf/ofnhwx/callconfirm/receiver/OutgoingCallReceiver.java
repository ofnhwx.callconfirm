package jp.gr.java_conf.ofnhwx.callconfirm.receiver;

import jp.gr.java_conf.ofnhwx.callconfirm.R;
import jp.gr.java_conf.ofnhwx.callconfirm.activity.confirm.CallConfirm;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.BluetoothTest;
import jp.gr.java_conf.ofnhwx.callconfirm.utility.info.AllowListInfo;
import jp.gr.java_conf.ofnhwx.olib.ErrorReporter;
import jp.gr.java_conf.ofnhwx.olib.utils.OPreference;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.widget.Toast;

/**
 * 電話発信を横取りするクラス.
 * @author yuta
 */
public class OutgoingCallReceiver extends BroadcastReceiver {

    // package com.android.phone.OutgoingCallBroadcaster
    public static final String EXTRA_ALREADY_CALLED = "android.phone.extra.ALREADY_CALLED";
    public static final String EXTRA_ORIGINAL_URI   = "android.phone.extra.ORIGINAL_URI"  ;

    /**
     * 発信確認の状態を取得.
     * @return true:確認済 / false:未確認
     */
    public static final boolean getStateConfirmed(Context context, String number) {
        OPreference prefs = OPreference.getInstance(context);
        String confirmed = prefs.getString(R.string.key_confirmed_number, null);
        if (confirmed == null) {
            return false;
        } else {
            return confirmed.equals(number);
        }
    }

    /**
     * 発信確認の状態を設定.
     */
    public static final void setStateConfirmed(Context context, String number) {
        OPreference prefs = OPreference.getInstance(context);
        prefs.write(R.string.key_confirmed_number, number);
    }

    /**
     * いろいろ確認して必要なら確認ダイアログを表示.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        ErrorReporter.initialize(context);
        OPreference prefs = OPreference.getInstance(context);
        String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
        String orguri = intent.getStringExtra(EXTRA_ORIGINAL_URI);
        // 確認済み(or 発信済み)ならフラグをリセットして通過
        if (intent.getBooleanExtra(EXTRA_ALREADY_CALLED, false) || getStateConfirmed(context, number)) {
            setStateConfirmed(context, null);
            return;
        }
        // 設定で無効になってるから何もしない
        if (!prefs.getBoolean(R.string.key_enable_callconfirm, false)) {
            return;
        }
        // スクリーンロックの確認
        if (prefs.getBoolean(R.string.key_screenlock, false)) {
            KeyguardManager km = (KeyguardManager)context.getSystemService(Context.KEYGUARD_SERVICE);
            if (km.inKeyguardRestrictedInputMode()) {
                return;
            }
        }
        // Bluetoothの確認
        if (prefs.getBoolean(R.string.key_bluetooth, false)) {
            if (BluetoothTest.isEnabled(context)) {
                return;
            }
        }
        // 許可リストに登録してあるなら無視
        if (prefs.getBoolean(R.string.key_use_allowlist, false)) {
            AllowListInfo info = new AllowListInfo(context, number);
            if (info.isAllowNumber()) {
                String name = info.getName();
                String text = TextUtils.isEmpty(name) ? PhoneNumberUtils.formatNumber(number) : name;
                Toast.makeText(context, text, Toast.LENGTH_LONG).show();
                return;
            }
        }
        // 一旦キャンセルして確認画面を表示
        if (!TextUtils.isEmpty(number)) {
            CallConfirm.show(context, number, orguri);
        }
        setResultData(null);
    }

}
